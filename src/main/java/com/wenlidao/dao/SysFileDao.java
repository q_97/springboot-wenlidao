package com.wenlidao.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wenlidao.entity.SysFile;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统文件持久层
 * @author INYXIN
 * @version 1.0.0
 */
@Mapper
public interface SysFileDao extends BaseMapper<SysFile> {
}
