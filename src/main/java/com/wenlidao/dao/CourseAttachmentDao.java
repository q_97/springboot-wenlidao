package com.wenlidao.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wenlidao.entity.course.CourseAttachment;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CourseAttachmentDao extends BaseMapper<CourseAttachment> {
}
