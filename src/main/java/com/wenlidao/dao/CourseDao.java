package com.wenlidao.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wenlidao.entity.course.Course;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface CourseDao extends BaseMapper<Course> {
    @Select("select nickname from sys_user where id=#{teacherId}")
    String getTeacher(Integer teacherId);
}
