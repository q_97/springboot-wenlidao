package com.wenlidao.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wenlidao.entity.course.CourseAction;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CourseActionDao extends BaseMapper<CourseAction> {
}
