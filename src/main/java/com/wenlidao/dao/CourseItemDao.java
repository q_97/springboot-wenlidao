package com.wenlidao.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wenlidao.entity.course.CourseItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CourseItemDao extends BaseMapper<CourseItem> {
}
