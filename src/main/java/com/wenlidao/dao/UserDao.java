package com.wenlidao.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wenlidao.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户持久层
 * @author INYXIN
 * @version 1.0.0
 */
@Mapper
public interface UserDao extends BaseMapper<User> {

}