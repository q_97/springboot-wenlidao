package com.wenlidao.service.user;

import cn.dev33.satoken.stp.StpUtil;
import com.wenlidao.common.api.R;
import com.wenlidao.common.dto.EmailLoginForm;
import com.wenlidao.common.dto.UserRegisterForm;
import com.wenlidao.common.dto.UsernameLoginForm;
import com.wenlidao.common.exception.AppException;
import com.wenlidao.common.vo.LoginResult;
import com.wenlidao.entity.User;
import me.zhyd.oauth.model.AuthUser;
import org.springframework.transaction.annotation.Transactional;

/**
 * 登录服务
 * @author INYXIN
 * @version 1.0.0
 * 3月 18, 2024   22:50
 */
@Transactional
public interface UserLoginService {

    /**
     * 检查用户
     * @param user 用户
     */
    default void checkUser(User user) {
        //根据disabled 判断user是否被封禁
        if (user.isDisabled()) {
            StpUtil.disable(user.getId(), -1);
        } else {
            StpUtil.untieDisable(user.getId());
        }
        StpUtil.checkDisable(user.getId());
    }

    /**
     * 核心做登录
     * @param user     用户
     * @param remember 记得
     * @return {@link R }<{@link LoginResult }>
     */
    R<LoginResult> doLogin(User user, boolean remember);

    /**
     * 通过用户名登录
     * @param usernameLoginForm 用户名登录表单
     * @return {@link R }<{@link LoginResult }>
     */

    R<LoginResult> login(UsernameLoginForm usernameLoginForm);

    /**
     * 通过邮箱登录
     * @param emailLoginForm 电子邮件登录表格
     * @return {@link R }<{@link LoginResult }>
     */
    R<LoginResult> login(EmailLoginForm emailLoginForm);

    /**
     * 通过第三方授权登录
     * @return {@link R }<{@link LoginResult }>
     */

    R<LoginResult> login(AuthUser authUser);

    /**
     * 获取登录信息
     * @return {@link R }<{@link LoginResult }>
     */
    R<LoginResult> getLoginInfo();

    /**
     * 注销(退出登录)
     * @return {@link R }
     */
    default R<Object> logout() {
        StpUtil.logout();
        return R.success();
    }

    /**
     * 注册
     * @param registerForm 注册表格
     * @return {@link User }
     */
    User register(UserRegisterForm registerForm) throws AppException;


}
