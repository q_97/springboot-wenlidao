package com.wenlidao.service.user;


import com.wenlidao.common.exception.AppException;
import com.wenlidao.entity.User;
import org.springframework.transaction.annotation.Transactional;

/**
 * 用户账号服务
 * @author INYXIN
 * @version 1.0.0
 * 3月 17, 2024   23:11
 */
@Transactional
public interface UserAccountService {
    /**
     * 登录类型 用户名
     */
    String LOGIN_TYPE_USERNAME = "username";
    /**
     * 登录类型 电子邮件
     */
    String LOGIN_TYPE_EMAIL = "email";
    /**
     * 登录类型电话
     */
    String LOGIN_TYPE_PHONE = "phone";

    /**
     * 是否拥有该类型账户
     * @param uid  UID
     * @param type 类型
     * @return boolean
     */
    boolean hasAccount(Integer uid, String type);

    /**
     * 删除帐号
     * @param uid  UID
     * @param type 类型
     */

    void deleteAccount(Integer uid, String type);

    /**
     * 更新帐户
     * @param uid        UID
     * @param type       类型
     * @param identifier 标识符
     */

    void updateAccount( Integer uid, String type, String identifier);

    /**
     * 按帐户获取用户
     * @param type       类型
     * @param identifier 标识符
     * @return
     */
    User getUserByAccount(String type, String identifier);

    /**
     * 是存在帐户
     * @param type       类型
     * @param identifier 标识符
     * @return boolean
     */
     boolean isExistsAccount(String type,String identifier);
    /**
     * 检查用户帐户是否已存在
     */
    void checkAndSetUserAccount(User user) throws AppException;
}
