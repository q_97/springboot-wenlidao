package com.wenlidao.service.user;


import com.wenlidao.common.dto.EmailLoginForm;
import com.wenlidao.common.dto.UserRegisterForm;
import com.wenlidao.entity.User;
import me.zhyd.oauth.model.AuthUser;
import org.springframework.transaction.annotation.Transactional;

/**
 * 用户自动注册服务
 * @author INYXIN
 * @version 1.0.0
 * 3月 18, 2024   13:35
 */
@Transactional
public interface UserAutoRegisterService {
    /**
     * 表单注册
     * @param registerForm 注册表格
     * @return {@link User }
     */
    User register(UserRegisterForm registerForm);

    /**
     * 邮箱自动注册
     * @param emailLoginForm 电子邮件登录表格
     * @return {@link User }
     */
    User register(EmailLoginForm emailLoginForm);

    /**
     * Oath2自动注册
     * @param authUser 身份验证用户
     * @return {@link User }
     */
    User register(AuthUser authUser);

}
