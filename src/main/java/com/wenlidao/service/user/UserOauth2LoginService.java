package com.wenlidao.service.user;


import jakarta.servlet.http.HttpServletResponse;
import me.zhyd.oauth.model.AuthCallback;

import java.io.IOException;

/**
 * 用户 OAuth2 登录服务
 * @author INYXIN
 * @version 1.0.0
 * 3月 18, 2024   22:50
 */
public interface UserOauth2LoginService extends UserLoginService {
    /**
     * oauth2 授权, 重定向到第三方登录页面
     * @param response 响应
     * @param source   源
     */
     void oauth2Authorize(HttpServletResponse response, String source) throws IOException;

    /**
     * oauth2 回调
     * @param source   源
     * @param callback 回调
     * @param response 响应
     */
    void oauth2Callback(String source, AuthCallback callback, HttpServletResponse response) throws IOException;
}
