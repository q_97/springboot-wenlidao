package com.wenlidao.service.user;


import com.baomidou.mybatisplus.extension.service.IService;
import com.wenlidao.common.api.R;
import com.wenlidao.common.dto.UpdatePasswordDTO;
import com.wenlidao.common.dto.UserFormDTO;
import com.wenlidao.entity.User;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface UserService extends IService<User>,
        UserOauth2LoginService,
        UserAccountService,
        UserAutoRegisterService {

    /**
     * 注册用户(创建用户),支持account
     * @param user 用户
     * @return {@link User }
     */

    User createUser(User user);

    /**
     * 更新用户(不支持直接修改account,请参考{@link UserAccountService#updateAccount(Integer, String, String)} (User)})
     * @param user 用户
     * @return {@link User }
     */
    User updateUser(User user);

    R<Void> updatePassword(UpdatePasswordDTO updatePasswordDTO);

    R<List<User>> getAllUser(UserFormDTO params);

    R<Boolean> delByIds(int[] ids);


    /**
     * 获取用户列表
     * @param tableQuery 表查询
     * @return {@link R }<{@link List }<{@link User }>>
     */
//    R<List<User>> getUserList(UserTableQuery tableQuery);


}
