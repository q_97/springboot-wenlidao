package com.wenlidao.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.wenlidao.common.api.R;
import com.wenlidao.entity.SysFile;
import com.wenlidao.entity.enumerate.SysFileCategory;
import org.springframework.core.io.UrlResource;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 系统文件业务类
 * @author INYXIN
 * @version 1.0.0
 * 2月 8, 2024   17:34
 */
@Transactional
public interface ISysFileService extends IService<SysFile> {
    /**
     * 上传文件
     * @param multipartFile 文件
     * @param category      类别
     * @return {@link String } 文件访问路径
     */
    SysFile upload(MultipartFile multipartFile, SysFileCategory category) ;

    /**
     * 下载文件
     * @param hash       散 列
     * @param isDownload 是否下载文件
     * @return {@link ResponseEntity }<{@link UrlResource }>
     */
    ResponseEntity<UrlResource> accessFile(String hash, boolean isDownload);

    /**
     * 删除文件
     * @param hash 文件的hash值
     * @return {@link R }<{@link Boolean }>
     */
    R<Boolean> deleteFile(String hash);

    /**
     * 我的文件
     * @return {@link R }<{@link List }<{@link SysFile }>>
     */
    R<List<SysFile>> myFiles();

    /**
     * 生成文件访问地址
     * @param sysFile sys 文件
     * @return {@link String }
     */
    String getAccessSrc(SysFile sysFile);

    /**
     * 获取文件
     * @param fileId 文件 ID
     * @return {@link SysFile }
     */
    SysFile getFile(Integer fileId);
}
