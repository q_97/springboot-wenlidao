package com.wenlidao.service;



import com.wenlidao.common.api.R;
import com.wenlidao.common.vo.ImageCaptcha;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

/**
 * 验证码服务
 * @author INYXIN
 * @since 1月 17, 2024   17:56
 */
@Transactional
public interface CaptchaService {
    /**
     * 创建图像验证码
     * @return
     */
    R<ImageCaptcha> createImageCaptcha() throws IOException;

    /**
     * 创建电子邮件验证码
     * @param email   电子邮件
     */
    R<Object> createEmailCaptcha(String email);


    /**
     * <pre>验证验证码</pre>
     * @param key     钥匙,可能是uuid 或 邮箱 或手机
     * @param captcha 验证码
     */
    void verifyCaptcha(String key,String captcha);

    /**
     * <div>验证码 DAO</div>
     * <div>将验证码保存到Redis</div>
     * @author INYXIN
     * @version 1.0.0
     * 2月 1, 2024   15:58
     */

    interface CaptchaDao {
        /** 验证码密钥前缀 */
        String CAPTCHA_KEY_PREFIX = "captcha:";

        /**
         * 保存验证码
         * @param key     键
         * @param value   值
         * @param seconds 保存时间 秒
         */
        void saveCaptcha(String key, String value, int seconds);

        /**
         * 按 ID 获取验证码
         * @param key 键
         * @return {@link String }
         */
        String getCaptchaByKey(String key);

        /**
         * 删除验证码
         * @param key 键
         * @return {@link Boolean }
         */
        Boolean removeCaptchaById(String key);
    }
}
