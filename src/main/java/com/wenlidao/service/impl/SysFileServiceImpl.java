package com.wenlidao.service.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wenlidao.common.api.R;
import com.wenlidao.common.exception.AppException;
import com.wenlidao.common.util.SaTokenUtil;
import com.wenlidao.dao.SysFileDao;
import com.wenlidao.entity.SysFile;
import com.wenlidao.entity.User;
import com.wenlidao.entity.enumerate.SysFileCategory;
import com.wenlidao.service.ISysFileService;
import com.wenlidao.service.user.UserService;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.UrlResource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * 系统文件服务
 * @author INYXIN
 * @version 1.0.0
 * 3月 28, 2024   19:16
 */
@Service
@Slf4j
public class SysFileServiceImpl extends ServiceImpl<SysFileDao, SysFile> implements ISysFileService {
    public static final String FILE_ACCESS_CONTEXT = "/file/";
    @Value ("${custom.file.cache-time}")
    long FILE_CACHE_TIME;
    @Value ("${custom.file.root-dir}")
    String FILE_ROOT_DIR;
    @Resource
    UserService userService;

    /**
     * 上传文件
     * @param multipartFile 文件
     * @param category      类别
     * @return {@link String } 文件访问路径
     */
    @Override
    public SysFile upload(MultipartFile multipartFile, SysFileCategory category) {
        if (multipartFile.isEmpty()) throw new AppException("文件为空");
        //1.读取文件基本信息
        final SysFile sysFile = readBasicInfo(multipartFile, category);
        //2.上传时,确定在磁盘上的真实文件
        final File file = getRealFile(sysFile);
        log.debug("上传文件 {} {}", sysFile, file);
        //3.保存sysFile 信息保存到数据库
        //3.1 查重
        final SysFile findByHash = lambdaQuery().eq(SysFile::getHash, sysFile.getHash()).one();
        if (Objects.nonNull(findByHash)) {
         return findByHash;
        }
        save(sysFile);
        //4. 将文件写入到硬盘(如果报错则事务回滚)
        try {
            multipartFile.transferTo(file);
        } catch(IOException e) {
            throw new AppException("文件上传失败" + e.getMessage());
        }
        //5.返回结果
        return  sysFile;
    }

    /**
     * 下载文件
     * @param hash       散 列
     * @param isDownload 是否下载文件
     * @return {@link ResponseEntity }<{@link UrlResource }>
     */
    @Override
    public ResponseEntity<UrlResource> accessFile(String hash, boolean isDownload) {
        final SysFile one = lambdaQuery().eq(SysFile::getHash, hash).one();
        log.debug("访问文件 {} {}", one, isDownload);
        if (one == null) {
            throw new AppException("文件不存在");
        }
        final File realFile = getRealFile(one);//找到真实的文件,获取磁盘上的真实文件
        if (!realFile.exists()) throw new AppException("文件丢失");
        UrlResource resource;
        try {
            resource = new UrlResource(realFile.toURI());
        } catch(MalformedURLException e) {
            throw new AppException("格式错误的 URL异常");
        }
        HttpHeaders headers = new HttpHeaders();
        //如果为下载模式,则开启附件,否则下载时文件将没有名字
        if (isDownload) {
            headers.setContentDispositionFormData("attachment", URLEncoder.encode(one.getFilename()));
        }
        //设置文件类型
        headers.set("Content-Type", one.getMimeType());
        // 设置缓存控制为7天
        CacheControl cacheControl = CacheControl.maxAge(FILE_CACHE_TIME, TimeUnit.SECONDS);
        return ResponseEntity.ok()
                             .headers(headers)
                             .cacheControl(cacheControl)
                             .body(resource);
    }


    /**
     * 删除文件
     * @param hash 文件hash值
     * @return {@link R }<{@link Boolean }>
     */
    @Override
    public R<Boolean> deleteFile(String hash) {
        final SysFile sysFile = lambdaQuery().eq(SysFile::getHash, hash).one();
        log.debug("删除文件: {}", sysFile);
        if (Objects.isNull(sysFile)) {
            throw new AppException("文件不存在");
        }
        final File realFile = getRealFile(sysFile);
//        final LambdaQueryChainWrapper<SysFile> eq = lambdaQuery().eq(SysFile::getHash, hash);
        final QueryWrapper<SysFile> hash1 = new QueryWrapper<SysFile>().eq("hash", hash);
        final boolean remove = remove(hash1);
        final boolean delete = realFile.delete();
        return R.success(remove && delete);
    }

    /**
     * 我的文件
     * @return {@link R }<{@link List }<{@link SysFile }>>
     */
    @Override
    public R<List<SysFile>> myFiles() {
        final int loginId = SaTokenUtil.getLoginId();
        final List<SysFile> list = lambdaQuery()
                .eq(SysFile::getUserId, loginId)
                .orderByAsc(SysFile::getTime)
                .list();
        return R.success(list);
    }

    /**
     * 读取 MultipartFile 文件基础信息
     * @param file     文件
     * @param category 文件分类
     * @return {@link SysFile }
     */
    private SysFile readBasicInfo(MultipartFile file, SysFileCategory category) {
        final SysFile appFile = new SysFile();
        //获取当前登录的用户ID
        final int loginId = SaTokenUtil.getLoginId();
        appFile.setUserId(loginId);
        //读取基本信息
        appFile.setMimeType(file.getContentType());//设置mime类型
        appFile.setSize(file.getSize());//设置文件大小
        appFile.setFilename(file.getOriginalFilename());//设置原始名字
        appFile.setCategory(category);//设置分类 (目录)
        appFile.setTime(new Date());        //设置上传时间
        //文件hash算法: md5(userId+filename),计算出唯一ID, 一个人不能上传同名的重复文件
        appFile.setHash(SecureUtil.md5(appFile.getUserId() + appFile.getFilename()));
        return appFile;
    }

    /**
     * 确定磁盘上的真实文件
     * @param sysFile sys 文件
     * @return {@link File }
     */
    public File getRealFile(SysFile sysFile) {
        //1.确定文件存储目录 : root/category
        final String label = sysFile.getCategory().getLabel();
        final File dir = new File(FILE_ROOT_DIR, label);
        if (!dir.exists() && !dir.mkdirs()) throw new AppException("创建文件存储目录失败");
        //2.生成文件名
        //2.1获取扩展名
        final String ext = FileUtil.extName(sysFile.getFilename());
        //2.2文件名算法为: hash+扩展名, 如 1e8c49dc9a1e97eb9193918f57dd90f3.png
        String filename = sysFile.getHash() + "." + ext;
        return new File(dir, filename);
    }

    /**
     * 动态生成资源访问路径  context/{hash}
     * @return {@link String }
     */
    public String getAccessSrc(SysFile sysFile) {
        final String src = FILE_ACCESS_CONTEXT + sysFile.getHash();
        log.debug("{}访问路径: {}", sysFile, src);
        return src;
    }

    /**
     * 获取文件
     * @param fileId 文件 ID
     * @return {@link SysFile }
     */
    @Override
    public SysFile getFile(Integer fileId) {
        final SysFile byId = getById(fileId);
        if (byId==null)return null;
        final User owner = userService.getById(byId.getUserId());
        byId.setOwner(owner);
        return byId;
    }
}
