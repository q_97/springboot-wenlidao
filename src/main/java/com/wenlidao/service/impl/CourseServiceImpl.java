package com.wenlidao.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Snowflake;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wenlidao.common.api.R;
import com.wenlidao.common.dto.CourseTableQueryDTO;
import com.wenlidao.common.exception.AppException;
import com.wenlidao.common.model.MahoutUtils;
import com.wenlidao.common.model.UserCourseRatingCalculator;
import com.wenlidao.common.util.SaTokenUtil;
import com.wenlidao.common.util.lucene.LuceneUtil;
import com.wenlidao.common.util.lucene.component.LuceneSearcher;
import com.wenlidao.dao.CourseActionDao;
import com.wenlidao.dao.CourseDao;
import com.wenlidao.entity.SysFile;
import com.wenlidao.entity.User;
import com.wenlidao.entity.course.Course;
import com.wenlidao.entity.course.CourseAction;
import com.wenlidao.entity.course.CourseAttachment;
import com.wenlidao.entity.enumerate.ActionType;
import com.wenlidao.entity.enumerate.SysFileCategory;
import com.wenlidao.entity.model.Customer;
import com.wenlidao.entity.model.UserActivity;
import com.wenlidao.service.ISysFileService;
import com.wenlidao.service.course.CourseItemService;
import com.wenlidao.service.course.CourseService;
import com.wenlidao.service.course.action.ActionService;
import com.wenlidao.service.course.action.IssueService;
import com.wenlidao.service.course.action.ReplyService;
import com.wenlidao.service.course.action.SelectedCourseService;
import com.wenlidao.service.user.UserService;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.Resource;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static com.wenlidao.entity.enumerate.ActionType.*;

/**
 * 课程服务业务实现类
 * @author INYXIN
 * @version 1.0.0
 * 4月 2, 2024   08:12
 */
@Schema(description = "课程服务业务实现类")
@Service
public class CourseServiceImpl extends ServiceImpl<CourseDao, Course> implements CourseService {
    @Schema(hidden = true)
    @Resource
    UserService userService;
    @Resource
    ISysFileService fileService;
    @Resource
    CourseItemService itemService;
    @Resource
    CourseDao courseDao;

    @Resource
    CourseActionDao courseActionDao;

    @Resource
    ActionService actionService;
    @Resource
    IssueService issueService;
    @Resource
    ReplyService replyService;
    @Resource
    SelectedCourseService selectedService;

    /**
     * 添加课程
     *
     * @param course 课程
     * @return {@link R }<{@link Course }>
     */
    @Override
    public R<Course> addCourse(Course course) {
        final int loginId = SaTokenUtil.getLoginId();
        course.setTeacherId(loginId)
              .setTime(new Date());

        boolean b =this.saveOrUpdate(course);
        if (b) {
            return R.success(course).msg("课程创建成功");
        }
        throw new AppException("课程创建失败");
    }

    /**
     * 更新课程
     *
     * @param course 课程
     * @return {@link R }<{@link Course }>
     */
    @Override
    public R<Course> updateCourse(Course course) {
        return null;
    }

    /**
     * 删除课程
     * @param id 同上
     * @return {@link R }<{@link Boolean }>
     */
    @Override
    public R<Boolean> deleteCourse(Integer id) {
        return null;
    }

    /**
     * 列出课程附件
     *
     * @param courseId 课程 ID
     * @return {@link R }<{@link List }<{@link CourseAttachment }>>
     */
    @Override
    public R<List<CourseAttachment>> listCourseAttachment(Integer courseId) {
        //1.查询课程id的附件
        final List<CourseAttachment> courseAttachments = new CourseAttachment()
                .selectList(new LambdaQueryWrapper<CourseAttachment>()
                        .eq(CourseAttachment::getCourseId, courseId));
        //2.遍历结果集,补全附件的文件信,课管理的课程信息
        courseAttachments.forEach(attachment -> {
            attachment.setFile(fileService.getFile(attachment.getFileId()));
            if (attachment.getCourseItemId() != null) {
                attachment.setCourseItem(itemService.getById(attachment.getCourseItemId()));
            }
        });
        return R.success(courseAttachments)
                .msg("查询成功")
                .put("count", courseAttachments.size());
    }

    /**
     * 添加已上传的文件为附件 (直接上传)
     *
     * @param courseId      课程 ID
     * @param courseItemId  文件 ID
     * @param category      分类
     * @param multipartFile 附件
     * @return {@link R }<{@link CourseAttachment }>
     */
    @Override
    public R<CourseAttachment> addAttachment(Integer courseId, Integer courseItemId, SysFileCategory category, MultipartFile multipartFile) {
        //1.先调用文件上传服务
        final SysFile sysFile = fileService.upload(multipartFile, category);
        //2.调用下面的方法,继续关联文件与课程
        return addAttachment(courseId, courseItemId, sysFile.getId());
    }

    /**
     * 添加已上传的文件为附件 (间接上传)
     *
     * @param courseId     课程 ID
     * @param courseItemId 课程项目 ID
     * @param fileId       文件 ID
     * @return {@link R }<{@link CourseAttachment }>
     */
    @Override
    public R<CourseAttachment> addAttachment(Integer courseId, Integer courseItemId, Integer fileId) {
        //1.创建课程附件
        final CourseAttachment courseAttachment = new CourseAttachment()
                .setCourseId(courseId)
                .setCourseItemId(courseItemId)
                .setFileId(fileId);
        //2.查询课程附件是否存在
        final boolean exists = courseAttachment.selectList(new LambdaQueryWrapper<CourseAttachment>()
                .eq(CourseAttachment::getCourseId, courseId)
                .eq(CourseAttachment::getFileId, fileId)).size() > 0;
        //2.1课程已存在同名资源
        if (exists) {
            //不允许同一个课程上传相同的文件
            throw new AppException("添加失败,该课程已存在同名资源");
        } else {
            //3向课程附件表中插入数据
            final boolean insert = courseAttachment.insert();
            if (!insert) throw new AppException("添加附件失败");
        }
        //4.补全附件数据
        courseAttachment.setFile(fileService.getFile(fileId));
        if (courseItemId != null) {
            courseAttachment.setCourseItem(itemService.getById(courseItemId));
        }
        //4.返回数据
        return R.success(courseAttachment).msg("添加附件成功");
    }

    /**
     * 后台课程列表条件查询
     * @param queryDTO
     * @return
     */
    @Override
    public R<List<Course>> selectAll(CourseTableQueryDTO queryDTO) {
        //查询所有课程
       PageHelper.startPage(queryDTO.getPageNum(), queryDTO.getPageSize());
        List<Course> list = lambdaQuery()
                .select(Course::getId, Course::getName, Course::getTeacherId, Course::getSubject)
                .eq(StringUtils.hasText(queryDTO.getCategory()),Course::getCategory,queryDTO.getCategory())
                .eq(StringUtils.hasText(queryDTO.getType()),Course::getType,queryDTO.getType())
                .eq(StringUtils.hasText(queryDTO.getSubject()),Course::getSubject,queryDTO.getSubject())
                .eq(StringUtils.hasText(queryDTO.getSchoolLeve()),Course::getSchool,queryDTO.getSchoolLeve())
                .like(StringUtils.hasText(queryDTO.getCourseName()),Course::getName,queryDTO.getCourseName())
                .list();
                list.forEach(item ->{
                    User user = userService.getById(item.getTeacherId());
                    if (Objects.nonNull(user)) {
                        item.setTeacherName(user.getNickname());
                    }
                });
        PageInfo<Course> coursePageInfo = new PageInfo<>(list);
        long total = coursePageInfo.getTotal();
        if (total==0){
            throw new AppException("没有查询到相关数据");
        }
        return R.success(list).put("count",total);
    }

    /**
     * 删除数据
     * @param ids
     * @return
     */
    @Override
    public R<Boolean> delete(int[] ids) {
        boolean b = removeBatchByIds(Arrays.stream(ids)
                .boxed()
                .collect(Collectors.toList()));
        if (!b){
            throw new AppException("删除失败");
        }
        return R.success(true);
    }

    /**
     * 根据课程ID查询课程
     * @param id 课程ID
     * @return {@link R }<{@link Course }>
     */
    @Override
    public R<Course> getCourse(Integer id) {
        final Course course = getById(id);
        if (course == null) throw new AppException("课程[" + id + "]不存在");
        final User teacher = userService.getById(course.getTeacherId());
        course.setTeacher(teacher);
        return R.success(course);
    }

    /**
     * 建立索引
     */
    @Override
    //每次清空旧的索引 , 当然也可以选择其他策略
//    @PostConstruct
    public void Indexing() {
        System.err.println("建立索引中....");
        try {
            LuceneUtil.getManager().deleteAllDocuments();
//            final Document[] documents = list().stream().map(course -> LuceneUtil.getConverter().convert(course)).toArray(Document[]::new);
            final List<Course> courses = list(); // 获取Course对象列表
            List<Document> documentsList = new ArrayList<>();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            courses.forEach(course -> {
                // 在这里修改course对象的属性值
                course.setTeacherName(courseDao.getTeacher(course.getTeacherId())); // 示例：修改名称属性

                //将datetime字段转换为字符串
                String dateTimeStr = formatter.format(course.getTime());

                // 然后将其转换为Document对象
                Document document = LuceneUtil.getConverter().convert(course);

                document.add(new Field("datetime", dateTimeStr, Field.Store.YES, Field.Index.ANALYZED)); // 根据需求选择存储和索引策略

                // 将转换后的Document对象添加到一个临时集合中
                documentsList.add(document);
            });

            // 最后将临时集合转换为Document数组
            final Document[] documents = documentsList.toArray(Document[]::new);
            LuceneUtil.getManager().addDocuments(documents);
            LuceneUtil.getManager().commit();
            System.err.println("索引建立完成.....");
        } catch (IOException e) {
            throw new AppException("索引构建异常");
        }
    }

    /**
     * 构建多条件搜索
     * @param search
     * @param category
     * @param type
     * @param subject
     * @param school
     * @param sort
     * @param page
     * @param size
     * @return
     */
    @Override
    public R<List<Map<String, String>>> homeSearch(String search, String category, String type, String subject, String school, String sort, Integer page, Integer size) {
        //1.获取自定义的Lucene搜索组件
        final LuceneSearcher searcher = LuceneUtil.getSearcher();
        //2.构建多值查询条件
        final BooleanQuery query = new BooleanQuery();
        try {
            if (!search.isEmpty()) {
                final Query query1 = new QueryParser("name", LuceneUtil.getAnalyzer()).parse(search);
                final Query query2 = new QueryParser("description", LuceneUtil.getAnalyzer()).parse(search);
                final Query query3 = new QueryParser("teacherName", LuceneUtil.getAnalyzer()).parse(search);
                query.add(query1, BooleanClause.Occur.SHOULD);
                query.add(query2, BooleanClause.Occur.SHOULD);
                query.add(query3, BooleanClause.Occur.SHOULD);
            }
            if (!category.isEmpty()) {
                query.add(new TermQuery(new Term("category", category)), BooleanClause.Occur.MUST);
            }
            if (!type.isEmpty()) {
                query.add(new TermQuery(new Term("type", type)), BooleanClause.Occur.MUST);
            }
            if (!subject.isEmpty()) {
                query.add(new TermQuery(new Term("subject", subject)), BooleanClause.Occur.MUST);
            }
            if (!school.isEmpty()) {
                query.add(new TermQuery(new Term("school", school)), BooleanClause.Occur.MUST);
            }
            if (category.isEmpty() && type.isEmpty() && subject.isEmpty() && school.isEmpty() && search.isEmpty()) {
                query.add(new WildcardQuery(new Term("name", "*")), BooleanClause.Occur.MUST);
            }
            //3.返回查询
            if (page == null) {
                page = 1;
            }
            if (size == null) {
                size = 100;
            }
            return searcher.search(query, page, size, sort);
        } catch (ParseException | IOException e) {
            throw new AppException("搜索引擎服务异常");
        }
    }

    /**
     * 计算用户对课程的评分
     * @param user_id
     * @param course_id
     * @return
     */
    @Override
    public Double getUserActivity(Integer user_id,Long course_id) {
        UserActivity userActivity = new UserActivity();
        // 合并查询
        QueryWrapper<CourseAction> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", user_id);
        queryWrapper.eq("course_id", course_id);
        List<CourseAction> actions = courseActionDao.selectList(queryWrapper);

// 统计不同类型的操作数量
        // 统计不同类型的操作数量
        Map<Object, Long> actionCounts = actions.stream()
                .collect(Collectors.groupingBy(CourseAction::getType, Collectors.counting()));

        userActivity.setViewCount(Math.toIntExact(actionCounts.getOrDefault(View, 0L)));
        userActivity.setSelectedCourses(Math.toIntExact(actionCounts.getOrDefault(Select, 0L)));
        userActivity.setQuestionsAsked(Math.toIntExact(actionCounts.getOrDefault(Issue, 0L)));
        userActivity.setAnswersProvided(Math.toIntExact(actionCounts.getOrDefault(Reply, 0L)));

        return new UserCourseRatingCalculator().calculateRating(userActivity,actionService.CountView(user_id),issueService.CountIssue(user_id),replyService.CountReply(user_id),selectedService.CountSelect(user_id));
    }

    /**
     * 获取推荐课程
     * @param user_id
     * @return
     */
    @Override
    public R<List<Map<String, String>>> getRecommendCourseByItems(Long user_id) {
        // mock 数据工具类
        Snowflake flake = new Snowflake();

        //1.获取自定义的Lucene搜索组件
        final LuceneSearcher searcher = LuceneUtil.getSearcher();

        List<CourseAction> actionList = courseActionDao.selectList(null);
        List<Customer> customerList = new ArrayList<>();
        try{
            for (int i = 0;i<actionList.size();i++) {
                if (i>0 && Objects.equals(actionList.get(i).getCourseId(), actionList.get(i - 1).getCourseId())){
                    continue;
                }
                Customer customer = new Customer();
                customer.setCustomerId(Long.valueOf(actionList.get(i).getUserId()));
                customer.setCommodityId(Long.valueOf(actionList.get(i).getCourseId()));
                customer.setId(Long.valueOf(actionList.get(i).getId()));
                double d = getUserActivity(actionList.get(i).getUserId(), Long.valueOf(actionList.get(i).getCourseId()));
                float value = (float) d;
                customer.setValue(value);
                customerList.add(customer);
            }
            Float maxValue = 0.01F;
            Long maxValueItem = null;
            Long maxValueUser = null;
            for (Customer customer : customerList) {
                if (Objects.equals(customer.getCustomerId(), user_id) && customer.getValue() > maxValue) {
                    maxValue = customer.getValue();
                    maxValueItem = customer.getCommodityId();
                    maxValueUser = customer.getUserId();
                }
            }
            if (maxValueItem==null){
                maxValueItem=customerList.get(0).getCommodityId();
            }

            final BooleanQuery query = new BooleanQuery();

            if (maxValueUser == null){
                query.add(new WildcardQuery(new Term("name", "*")), BooleanClause.Occur.MUST);
                return searcher.search(query,1,10,"Latest");
            }else {
                Map<Long,Float> itemRecommends = MahoutUtils.itemModel(customerList,maxValueItem);

                if(itemRecommends.isEmpty()){
                    query.add(new WildcardQuery(new Term("name", "*")), BooleanClause.Occur.MUST);
                    return searcher.search(query,1,10,"Latest");
                }else {
                    List<Map.Entry<Long,Float>> list = new ArrayList<>(itemRecommends.entrySet());
                    list.sort(Map.Entry.comparingByValue());
                    CollUtil.reverse(list);
                    List<Long> list1 = new ArrayList<>();
                    for (Map.Entry<Long,Float> entry:list){
                        list1.add(entry.getKey());
                    }
                    return searcher.PreciseQueries(list1);
            }

            }

        }catch (Exception e){
            throw new AppException("搜索引擎服务异常");
        }
    }

    /**
     * 首页展示
     * @param obj
     * @param objVal
     * @param size
     * @return
     */
    @Override
    public R<List<Map<String, String>>> display(String obj, String objVal, Integer size) {
        //1.获取自定义的Lucene搜索组件
        final LuceneSearcher searcher = LuceneUtil.getSearcher();
        //2.构建多值查询条件
        final BooleanQuery query = new BooleanQuery();

        Integer page = 1;
        String sort = "综合排序";

        try {
            if (!objVal.isEmpty() && !obj.isEmpty() && !size.equals(0)) {
                query.add(new TermQuery(new Term(obj, objVal)), BooleanClause.Occur.MUST);
            }
            return searcher.search(query, page, size, sort);
        } catch (IOException e) {
            throw new AppException("搜索引擎服务异常");
        }
    }

    @Override
    public R<List<Course>> getPublish() {
        final int loginId = SaTokenUtil.getLoginId();
        final List<Course> list = lambdaQuery().eq(Course::getTeacherId, loginId).list();
        list.forEach(item -> {
            if (Objects.nonNull(item.getTeacherId())) {
                User byId = userService.getById(item.getTeacherId());
                if (Objects.nonNull(byId)) {
                    item.setTeacherName(byId.getNickname());
                }
            }
        });
        return R.success(list).msg("查询成功");
    }
}
