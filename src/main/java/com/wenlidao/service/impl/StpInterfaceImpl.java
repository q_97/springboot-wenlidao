package com.wenlidao.service.impl;

import cn.dev33.satoken.stp.StpInterface;
import com.wenlidao.entity.User;
import com.wenlidao.service.user.UserService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/**
 * Sa-token STP 接口 impl
 * @author INYXIN
 * @version 1.0.0
 * 3月 28, 2024   22:40
 */
@Service
public class StpInterfaceImpl implements StpInterface {
    @Resource
    UserService userService;

    /**
     * 返回指定账号id所拥有的权限码集合
     * @param loginId   账号id
     * @param loginType 账号类型
     * @return 该账号id具有的权限码集合
     */
    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        return null;
    }

    /**
     * 返回指定账号id所拥有的角色标识集合
     * @param loginId   账号id
     * @param loginType 账号类型
     * @return 该账号id具有的角色标识集合
     */
    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        final int userId = Integer.parseInt(loginId.toString());
        final User one = userService.lambdaQuery()
                                    .select(User::getRole)
                                    .eq(User::getId, userId)
                                    .one();
        if (one != null) return Collections.singletonList(one.getRole().getValue());
        return null;
    }
}
