package com.wenlidao.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wenlidao.common.api.R;
import com.wenlidao.common.exception.AppException;
import com.wenlidao.dao.CourseItemDao;
import com.wenlidao.entity.course.Course;
import com.wenlidao.entity.course.CourseItem;
import com.wenlidao.entity.enumerate.DictEnum;
import com.wenlidao.service.course.CourseItemService;
import com.wenlidao.service.course.CourseService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 课程项业务实现类
 * @author INYXIN
 * @version 1.0.0
 * 4月 6, 2024   20:45
 */
@Service
public class CourseItemServiceImpl extends ServiceImpl<CourseItemDao, CourseItem> implements CourseItemService {
    @Resource
    CourseService courseService;

    /**
     * 课程项下拉选择框数据
     * @param courseId 课程 ID
     * @return {@link R }<{@link List }<{@link DictEnum }>>
     */
    @Override
    public R<List<DictEnum>> selectionCourseItem(Integer courseId) {
        final List<DictEnum> selectList = lambdaQuery()
                .eq(CourseItem::getCourseId, courseId)
                .list().stream().map(DictEnum.class::cast).toList();
        return R.success(selectList);
    }

    /**
     * 获取课程和项目
     * @param courseId 课程 ID
     * @return {@link R }<{@link Course }>
     */
    @Override
    public R<Course> getCourseAndItems(Integer courseId) {
        final R<Course> result = courseService.getCourse(courseId);
        final Course course = result.data;
        if (course == null) throw new AppException("课程不存在");
        final List<CourseItem> list = lambdaQuery()
                .eq(CourseItem::getCourseId, courseId)
                .orderByDesc(CourseItem::getTime)
                .list();
        course.setItems(list);
        return R.success(course);
    }
}
