package com.wenlidao.service.impl;

import cn.dev33.satoken.secure.BCrypt;
import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wenlidao.common.api.R;
import com.wenlidao.common.dto.*;
import com.wenlidao.common.exception.AppException;
import com.wenlidao.common.util.SaTokenUtil;
import com.wenlidao.common.vo.LoginResult;
import com.wenlidao.dao.UserDao;
import com.wenlidao.entity.User;
import com.wenlidao.entity.course.Course;
import com.wenlidao.entity.enumerate.Gender;
import com.wenlidao.entity.enumerate.Role;
import com.wenlidao.service.CaptchaService;
import com.wenlidao.service.user.UserAccountService;
import com.wenlidao.service.user.UserService;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import me.zhyd.oauth.model.AuthCallback;
import me.zhyd.oauth.model.AuthResponse;
import me.zhyd.oauth.model.AuthUser;
import me.zhyd.oauth.request.AuthDefaultRequest;
import me.zhyd.oauth.request.AuthRequest;
import me.zhyd.oauth.utils.AuthStateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserServiceImpl extends ServiceImpl<UserDao, User> implements UserService {
    @Value ("${custom.front.ip}")
    String FRONT_IP;
    @Value ("${custom.front.port}")
    String FRONT_PORT;
    @Resource
    CaptchaService captchaService;
    @Autowired
    Map<String, AuthDefaultRequest> oauth2Providers;

    /**
     * 用户是否拥有该类型账户
     * @param uid  UID
     * @param type 类型
     * @return boolean
     */
    @Override
    public boolean hasAccount(Integer uid, String type) {
        final User one = lambdaQuery().eq(User::getId, uid).apply("JSON_CONTAINS_PATH(accounts,'one',{0})", "$." + type).one();
        return Objects.nonNull(one);
    }

    /**
     * 解绑帐号 JSON_REMOVE
     * @param uid  UID
     * @param type 类型
     */
    @Override
    public void deleteAccount(Integer uid, String type) {
        if (!lambdaQuery().eq(User::getId, uid).exists()) throw new AppException("用户不存在");
        final boolean update = lambdaUpdate().eq(User::getId, uid).setSql(String.format("accounts = JSON_REMOVE(accounts,'$.%s')", type)).update();
        if (!update) throw new AppException("解绑失败");
    }

    /**
     * 更新帐户 JSON_SET
     * @param uid        UID
     * @param type       类型
     * @param identifier 标识符
     */
    @Override
    public void updateAccount(Integer uid, String type, String identifier) {
        //1.判断是否有其他用户绑定了此账号
        if (isExistsAccount(type, identifier)) throw new AppException("该账户已存在");
        if (!lambdaQuery().eq(User::getId, uid).exists()) throw new AppException("用户不存在");
        //2.更新账户
        final boolean update = lambdaUpdate().eq(User::getId, uid).setSql(String.format("accounts = JSON_SET(accounts,'$.%s','%s')", type, identifier)).update();
        if (!update) throw new AppException("绑定失败");
    }

    /**
     * 按帐户获取用户
     * @param type       类型
     * @param identifier 标识符
     * @return
     */
    @Override
    public User getUserByAccount(String type, String identifier) {
        return lambdaQuery().comment("根据帐户查询用户").apply("JSON_UNQUOTE(JSON_EXTRACT(accounts, {0}))  = {1}", "$." + type, identifier).one();
    }

    /**
     * 是存在帐户
     * @param type       类型
     * @param identifier 标识符
     * @return boolean
     */
    @Override
    public boolean isExistsAccount(String type, String identifier) {
        return lambdaQuery().apply("JSON_UNQUOTE(JSON_EXTRACT(accounts, {0}))  = {1}", "$." + type, identifier).exists();
    }

    /**
     * 核心做登录
     * @param user     用户
     * @param remember 记得
     * @return {@link R }<{@link LoginResult }>
     */
    @Override
    public R<LoginResult> doLogin(User user, boolean remember) {
        checkUser(user);//检查账户是否被禁用,禁用则抛出异常,终止登录
        StpUtil.login(user.getId(), remember);
        final SaTokenInfo tokenInfo = StpUtil.getTokenInfo();
        user = getById(user.getId());
        return R.success(new LoginResult(tokenInfo, user));
    }

    /**
     * 通过用户名登录
     * @param loginForm 用户名登录表单
     * @return {@link R }<{@link LoginResult }>
     */
    @Override
    public R<LoginResult> login(UsernameLoginForm loginForm) {
        //1.校验验证码
        captchaService.verifyCaptcha(loginForm.getUuid(), loginForm.getCaptcha());
        //2.查询用户
        final User user = lambdaQuery().eq(User::getUsername, loginForm.getUsername()).one();
        if (user == null) {
            throw new AppException("用户不存在");
        }
        //3.校验密码
        System.err.println(BCrypt.hashpw(loginForm.getPassword()));
        if (!BCrypt.checkpw(loginForm.getPassword(), user.getPassword())) {
            throw new AppException("密码错误");
        }
//        if (!loginForm.getPassword().equals(user.getPassword())){
//            throw new AppException("密码错误");
//        }
        return doLogin(user, loginForm.isRememberMe());
    }

    /**
     * 通过邮箱登录
     * @param emailLoginForm 电子邮件登录表格
     * @return {@link R }<{@link LoginResult }>
     */
    @Override
    public R<LoginResult> login(EmailLoginForm emailLoginForm) {
        //1.校验邮箱验证码
        final String email = emailLoginForm.getEmail();
        final String captcha = emailLoginForm.getCaptcha();
        captchaService.verifyCaptcha(email, captcha);
        //2.判断邮箱是否已经注册
        User user = getUserByAccount(LOGIN_TYPE_EMAIL, email);
        if (user == null) {
            user = register(emailLoginForm);
        }
        return doLogin(user, emailLoginForm.isRememberMe());
    }

    /**
     * 通过第三方授权登录
     * @param authUser
     * @return {@link R }<{@link LoginResult }>
     */
    @Override
    public R<LoginResult> login(AuthUser authUser) {
        //1.校验该账号是否在本系统注册过
        final String identifier = authUser.getUuid();
        final String source = authUser.getSource();
        User user = getUserByAccount(source, identifier);
        //账户不存在自动注册
        if (user == null) {
            user = register(authUser);
        }
        return doLogin(user, true);
    }

    /**
     * 获取当前登录信息
     * @return {@link R }<{@link LoginResult }>
     */
    @Override
    public R<LoginResult> getLoginInfo() {
        final long uid = SaTokenUtil.getLoginId();
        final User user = getById(uid);
        final SaTokenInfo tokenInfo = StpUtil.getTokenInfo();
        return R.success(new LoginResult(tokenInfo, user));
    }

    /**
     * 注册
     * @param registerForm 注册表格
     * @return {@link User }
     */
    @Override
    public User register(UserRegisterForm registerForm) throws AppException {
        final String captcha = registerForm.getCaptcha();
        final String uuid = registerForm.getUuid();
        //1.校验验证码
        captchaService.verifyCaptcha(uuid, captcha);
        //2.开始注册
        final User user = new User();
        user.setUsername(registerForm.getUsername());
        user.setPassword(BCrypt.hashpw(registerForm.getPassword()));
        user.setNickname(registerForm.getNickname());
        final HashMap<String, String> accounts = new HashMap<>();
        accounts.put(LOGIN_TYPE_USERNAME, registerForm.getUsername());
        user.setAccounts(accounts);
        return createUser(user);
    }



    /**
     * 邮箱自动注册
     * @param emailLoginForm 电子邮件登录表格
     * @return {@link User }
     */
    @Override
    public User register(EmailLoginForm emailLoginForm) {
        final String email = emailLoginForm.getEmail();
        log.info("邮箱 {} 登录, 自动注册", email);
        //3.2 未注册, 自动注册
        User user = new User();
        user.setUsername(UUID.randomUUID().toString());
        user.setGender(Gender.UNKNOWN);
        user.setPassword(BCrypt.hashpw(email, BCrypt.gensalt()));
        user.setNickname(email);
        final HashMap<String, String> accounts = new HashMap<>();
        accounts.put(LOGIN_TYPE_EMAIL, email);
        user.setAccounts(accounts);
        return createUser(user);
    }

    /**
     * Oath2自动注册
     * @param authUser 身份验证用户
     * @return {@link User }
     */
    @Override
    public User register(AuthUser authUser) {
        final String source = authUser.getSource();
        final String identifier = authUser.getUuid();
        log.info("{} 类型账号 {} 未在本系统注册过, 准备进行注册 info={}", source, identifier, authUser);
        final User user = new User();
        user.setUsername(UUID.randomUUID().toString());
        user.setPassword(BCrypt.hashpw(authUser.getUuid(), BCrypt.gensalt()));
        user.setNickname(authUser.getNickname());
        final HashMap<String, String> accounts = new HashMap<>();
        accounts.put(source, identifier);
        user.setAccounts(accounts);
        user.setGender(Gender.valueOf(authUser.getGender().name()));
        user.setLocation(authUser.getLocation());
        user.setAvatar(authUser.getAvatar());
        //TOTO:以后扩展
        user.setRemark(authUser.getRemark());
        return createUser(user);
    }

    /**
     * 注册用户(创建用户)
     *<br/>
     * 注册时支持传入多个帐号,默认必有一个username帐号
     * @param user 用户
     * @return {@link User }
     */
    @Override
    public User createUser(User user) {
        //1.检查用户帐号是否重复
        checkAndSetUserAccount(user);
        //2.设置一些默认参数值
        if (!StringUtils.hasText(user.getRemark())) {
            //默认描述
            user.setRemark("空空如也,来说点什么吧~");
        }
        if (!StringUtils.hasText(user.getPassword())) {
            //默认密码
            user.setPassword(BCrypt.hashpw("123456"));
        }

        //默认状态 : 启用
        user.setDisabled(false);
        user.setRole(Role.User);
//        user.setRegisterTime(System.currentTimeMillis());
        //3.添加用户,将记录插入到数据库
        if (!this.save(user)) {
            log.debug("创建用户失败user={}", user);
            throw new AppException("创建用户失败");
        }
        return user;
    }

    /**
     * 更新用户(不支持直接修改account,请参考{@link UserAccountService#updateAccount(Integer, String, String)} (User)})
     * @param user 用户
     * @return {@link User }
     */
    @Override
    public User updateUser(User user) {
        user.setAccounts(null);
        log.debug("更新用户user={}", user);
        if (!updateById(user)) throw new AppException("修改失败");
        return user;
    }

    @Override
    public R<Void> updatePassword(UpdatePasswordDTO updatePasswordDTO) {
        //获取当前用户
        int loginId = SaTokenUtil.getLoginId();
        User user = this.getById(loginId);
        //验证密码
        if (BCrypt.checkpw(updatePasswordDTO.getPassword(), user.getPassword())){
            //更新密码
            user.setPassword(BCrypt.hashpw(updatePasswordDTO.getNewPassword()));
            if(!this.updateById(user))
                throw new AppException("修改失败");
            return R.success();
//            return R.fail().msg("密码错误");
//            throw new AppException("旧密码错误");
        }else {
            throw new AppException("旧密码错误");
        }
    }

    @Override
    public R<List<User>> getAllUser(UserFormDTO params) {
        PageHelper.startPage(params.getPageNum()
                , params.getPageSize());
        List<User> list = lambdaQuery()
                .select(User::getId,User::getUsername,User::getNickname,User::getGender,User::getAvatar,User::getLocation,User::getRemark,User::getRole)
                .like(StringUtils.hasText(params.getUsername()), User::getUsername, params.getUsername())
                .like(StringUtils.hasText(params.getUsername()), User::getNickname, params.getUsername())
                .list();
        PageInfo<User> userPageInfo = new PageInfo<>(list);
        long total = userPageInfo.getTotal();
        return R.success(list).put("count",total);
    }

    @Override
    public R<Boolean> delByIds(int[] ids) {
        boolean b = removeBatchByIds(Arrays.stream(ids)
                .boxed()
                .collect(Collectors.toList()));
        if (!b) {
            throw new AppException("删除失败");
        }
        return R.success(true);
    }

    /**
     * 检查并设置用户基础帐号
     * @param user 用户
     */
    @Override
    public void checkAndSetUserAccount(User user) {
        //1.为accounts添加默认值
        if (user.getAccounts() == null) {
            user.setAccounts(new HashMap<>());
        }
        //2.默认添加账户类型 USERNAME
        final HashMap<String, String> accounts = user.getAccounts();
        accounts.put(LOGIN_TYPE_USERNAME, user.getUsername());
        //3.遍历帐号集,进行重复校验,如果重复,则抛出异常结束
        accounts.keySet().forEach(type -> {
            final String identifier = accounts.get(type);
            if (isExistsAccount(type, identifier)) {
                throw new AppException("账户类型[ " + type + " ]  [ " + identifier + " ]已存在,用户注册失败");
            }
        });
    }

    /**
     * oauth2 授权, 重定向到第三方登录页面
     * @param response 响应
     * @param source   源
     */
    @Override
    public void oauth2Authorize(HttpServletResponse response, String source) throws IOException {
        final AuthDefaultRequest request = oauth2Providers.get(source);
        if (request == null) throw new AppException("不支持的OAuth2.0提供商：" + source);
        response.sendRedirect(request.authorize(AuthStateUtils.createState()));
    }

    /**
     * oauth2 回调
     * @param source   源
     * @param callback 回调
     * @param response 响应
     */
    @Override
    public void oauth2Callback(String source, AuthCallback callback, HttpServletResponse response) throws IOException {
        final String errorAddr = String.format("http://%s:%s/#/error", FRONT_IP, FRONT_PORT);
        final AuthRequest authRequest = oauth2Providers.get(source);
        if (authRequest == null) throw new AppException("不支持的OAuth2.0提供商：" + source);
        //用token换access-token进行登录
        final AuthResponse loginResponse = authRequest.login(callback);
        final int code = loginResponse.getCode();
        if (code != 2000) { //登录失败重定向到前台错误页面
            response.sendRedirect(String.format(errorAddr + "?error=%s&code=%s", URLEncoder.encode("授权失败", StandardCharsets.UTF_8), code));
            return;
        }
        final AuthUser authUser = (AuthUser) loginResponse.getData();
        final R<LoginResult> login = login(authUser);
        final SaTokenInfo token = login.data.getToken();
        final String format = String.format("http://%s:%s/?token=%s&isLogin=%s#/login", FRONT_IP, FRONT_PORT, token.tokenValue, token.isLogin);
        response.sendRedirect(format);
    }


}
