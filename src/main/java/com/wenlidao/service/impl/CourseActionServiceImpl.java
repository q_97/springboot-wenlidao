package com.wenlidao.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wenlidao.common.api.R;
import com.wenlidao.common.exception.AppException;
import com.wenlidao.common.util.SaTokenUtil;
import com.wenlidao.common.util.lucene.LuceneUtil;
import com.wenlidao.common.util.lucene.component.LuceneSearcher;
import com.wenlidao.common.util.sensitive.word.SpringSensitiveService;
import com.wenlidao.common.vo.course.CourseIssue;
import com.wenlidao.common.vo.course.CourseReply;
import com.wenlidao.dao.CourseActionDao;
import com.wenlidao.entity.User;
import com.wenlidao.entity.course.Course;
import com.wenlidao.entity.course.CourseAction;
import com.wenlidao.entity.enumerate.ActionType;
import com.wenlidao.entity.enumerate.Role;
import com.wenlidao.service.course.CourseService;
import com.wenlidao.service.course.action.ActionService;
import com.wenlidao.service.user.UserService;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.WildcardQuery;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 课程互动实现类
 *
 * @author INYXIN
 * @version 1
 * .0
 * .0
 * 4月 2, 2024   08:12
 */
@Schema(description = "课程互动实现类 4月 2, 2024   08:12")
@Service
@Slf4j
public class CourseActionServiceImpl extends ServiceImpl<CourseActionDao, CourseAction> implements ActionService {
    @Schema(hidden = true)
    @Resource
    UserService userService;
    @Resource
    CourseService courseService;
    @Resource
    SpringSensitiveService sensitiveService;

    @Resource
    CourseActionDao courseActionDao;

    /**
     * 更新课程进度进度
     *
     * @param courseId 课程 ID
     * @param itemId   课程项 ID
     * @return {@link R }<{@link Void }>
     */
    @Override
    public R<Void> updateProgress(Integer courseId, Integer itemId) {
        final int loginId = SaTokenUtil.getLoginId();
        //先查询课程进度
        final CourseAction one = getProgress(courseId);
        if (one != null) {
            one.setTime(new Date());
            one.setTargetId(itemId);
            updateById(one);
        } else {
            final CourseAction process = new CourseAction(ActionType.Progress).setCourseId(courseId).setTargetId(itemId).setUserId(loginId).setTime(new Date());
            save(process);
        }
        return R.success();
    }

    /**
     * 获取进度
     *
     * @param courseId 课程 ID
     * @return {@link R }<{@link CourseAction }>
     */
    @Override
    public CourseAction getProgress(Integer courseId) {
        final int loginId = SaTokenUtil.getLoginId();
        return lambdaQuery().eq(CourseAction::getType, ActionType.Progress).eq(CourseAction::getCourseId, courseId).eq(CourseAction::getUserId, loginId).one();
    }

    /**
     * 记录浏览行为
     *
     * @param courseId 课程id
     * @return {@link R }<{@link Void }>
     */
    @Override
    public R<Void> recordView(Integer courseId) {
        final CourseAction view = new CourseAction(ActionType.View)

                .setCourseId(courseId)

                .setUserId(SaTokenUtil

                        .getLoginId())

                .setTime(new Date());
        save(view);
        return R.success();
    }

    /**
     * 统计课程互动元数据
     *
     * @param courseId 课程 ID
     * @return {@link R }<{@link Map }<{@link ActionType }, {@link Long }>>
     */
    @Override
    public R<Map<ActionType, Long>> getCourseActionMetaData(Integer courseId) {
        final Map<ActionType, Long> map = lambdaQuery().select(CourseAction::getType).eq(CourseAction::getCourseId, courseId).list().stream().collect(Collectors.groupingBy(CourseAction::getType, Collectors.counting()));
        return R.success(map);
    }

    @Override
    public Long CountView(Integer user_id) {
        QueryWrapper<CourseAction> qe = new QueryWrapper<>();
        qe.eq("type", "View");
        qe.eq("user_id", user_id);
        return courseActionDao.selectCount(qe);
    }

    /**
     * 添加公告
     *
     * @param courseId
     * @param content  内容
     * @return {@link R }<{@link CourseAction }>
     */
    @Override
    public R<CourseAction> addAnnouncement(Integer courseId, String content) {
        sensitiveService.checkSensitive(content);
        final CourseAction courseAction = new CourseAction(ActionType.Notice).setUserId(SaTokenUtil.getLoginId()).setCourseId(courseId).setContent(content).setTime(new Date());
        final boolean save = save(courseAction);
        return R.success(courseAction);
    }

    /**
     * 获取课程公告列表
     *
     * @param courseId 课程 ID
     * @return {@link R }<{@link List }<{@link CourseAction }>>
     */
    @Override
    public R<List<CourseAction>> getAnnouncements(Integer courseId) {
        checkDataPermission(courseId);
        final List<CourseAction> list = lambdaQuery().eq(CourseAction::getType, ActionType.Notice).eq(CourseAction::getCourseId, courseId).list();
        return R.success(list);
    }

    /**
     * 获取我的公告通知
     *
     * @return {@link R }<{@link List }<{@link CourseAction }>>
     */
    @Override
    public R<List<CourseAction>> getNoticeAnnouncements() {
        return null;
    }

    /**
     * 更新课程公告
     *
     * @param noticeId 通知 ID
     * @param courseId
     * @param content  内容
     * @return {@link R }<{@link CourseAction }>
     */
    @Override
    public R<CourseAction> updateAnnouncement(Integer noticeId, Integer courseId, String content) {
        sensitiveService.checkSensitive(content);
        checkDataPermission(courseId);
        final CourseAction courseAction = new CourseAction(ActionType.Notice).setId(noticeId).setUserId(SaTokenUtil.getLoginId()).setCourseId(courseId).setContent(content).setTime(new Date());
        final boolean save = updateById(courseAction);
        return R.success(courseAction);
    }

    /**
     * 删除课程公告
     *
     * @param id 同上
     * @return {@link R }<{@link Boolean }>
     */
    @Override
    public R<Boolean> deleteAnnouncement(Integer id) {
        checkDataPermission(id);
        final boolean remove = remove(new LambdaQueryWrapper<CourseAction>().eq(CourseAction::getId, id).eq(CourseAction::getType, ActionType.Notice));
        return R.success(remove);
    }

    private void checkDataPermission(Integer courseID) {
        final User currenUser = userService.getById(SaTokenUtil.getLoginId());
        if (currenUser == null) throw new AppException("用户不存在");
        if (currenUser.getRole() == Role.Admin) return;//若是管理员直接跳过
        final CourseAction one = lambdaQuery().select(CourseAction::getUserId).eq(CourseAction::getId, courseID).one();
        if (currenUser.getId().equals(one.getUserId())) return;//数据归属正确,跳过
        throw new AppException("您无权操作该数据");
    }

    /**
     * 选择课程
     *
     * @param courseId 课程 ID
     * @return {@link R }<{@link Boolean }>
     */
    @Override
    public R<Void> selectedCourse(Integer courseId) {
        //检查是否已选,
        int loginId = SaTokenUtil.getLoginId();
        final boolean exists = lambdaQuery().eq(CourseAction::getType, ActionType.Select).eq(CourseAction::getUserId, loginId).eq(CourseAction::getCourseId, courseId).exists();
        final R<Void> success = R.success();
        if (!exists) {
            final CourseAction selected = new CourseAction(ActionType.Select).setTime(new Date()).setCourseId(courseId).setUserId(SaTokenUtil.getLoginId());
            save(selected);
            success.msg("选课成功");
        }
        return success;
    }

    /**
     * 取消选课
     *
     * @param courseId 课程 ID
     * @return {@link R }<{@link Boolean }>
     */
    @Override
    public R<Void> cancelSelected(Integer courseId) {
        //检查是否选择了该课程
        int loginId = SaTokenUtil.getLoginId();
        final boolean exists = lambdaQuery().eq(CourseAction::getType, ActionType.Select).eq(CourseAction::getUserId, loginId).eq(CourseAction::getCourseId, courseId).exists();
        final R<Void> success = R.success();
        final R<Void> fail = R.error();
        //选择了该课程才能取消
        if (exists) {
            final boolean remove = remove(new LambdaQueryWrapper<CourseAction>().eq(CourseAction::getType, ActionType.Select).eq(CourseAction::getUserId, loginId).eq(CourseAction::getCourseId, courseId));
            if (remove) {
                success.msg("退课成功");
            } else {
                fail.msg("退课失败");
            }
        }
        return success;
    }

    /**
     * 查询当前用户已选课程
     *
     * @return {@link R }<{@link List }<{@link Course }>>
     */
    @Override
    public R<List<Course>> listSelectedCourse() {
        final int loginId = SaTokenUtil.getLoginId();
        List<Integer> collect = lambdaQuery()
                .select(CourseAction::getCourseId)
                .eq(CourseAction::getType, ActionType.Select)
                .eq(CourseAction::getUserId, loginId)
                .list().stream().map(CourseAction::getCourseId).collect(Collectors.toList());
        if (collect.isEmpty()) return R.success(new ArrayList<>());
        List<Course> data = courseService.listByIds(collect);
        data.forEach(item -> {
            if (Objects.nonNull(item.getTeacherId())) {
                User byId = userService.getById(item.getTeacherId());
                if (Objects.nonNull(byId)) {
                    item.setTeacherName(byId.getNickname());
                }
            }
        });
        return R.success(data);
    }

    @Override
    public Long CountSelect(Integer user_id) {
        QueryWrapper<CourseAction> qe = new QueryWrapper<>();
        qe.eq("type", "Select");
        qe.eq("user_id", user_id);
        return courseActionDao.selectCount(qe);
    }


    /**
     * 添加课程问题
     *
     * @param courseId 课程 ID
     * @param content  内容
     * @return {@link R }<{@link CourseIssue }>
     */
    @Override
    public R<CourseIssue> addIssue(Integer courseId, String content) {
        sensitiveService.checkSensitive(content);
        final CourseAction issue = new CourseAction(ActionType.Issue).setTime(new Date()).setCourseId(courseId).setContent(content).setUserId(SaTokenUtil.getLoginId());
        if (!save(issue)) throw new AppException("发布Issue失败");
        final User actionUser = getActionUser(issue.getUserId());
        final CourseIssue courseIssue = new CourseIssue().setId(issue.getId()).setContent(issue.getContent()).setTime(issue.getTime()).setCount(0).setUsername(actionUser.getUsername()).setNickname(actionUser.getNickname()).setAvatar(actionUser.getAvatar());
        return R.success(courseIssue).msg("发布成功");
    }

    /**
     * 查询课程的问题
     *
     * @param courseId 课程 ID
     * @return {@link R }<{@link List }<{@link CourseIssue }>>
     */
    @Override
    public R<List<CourseIssue>> getIssues(Integer courseId) {
        final List<CourseIssue> collect = lambdaQuery().eq(CourseAction::getType, ActionType.Issue).eq(CourseAction::getCourseId, courseId).list().stream().map(action -> {
            User one = getActionUser(action.getUserId());
            return new CourseIssue().setId(action.getId()).setTime(action.getTime()).setContent(action.getContent()).setUserId(action.getUserId()).setUsername(one.getUsername()).setNickname(one.getNickname()).setAvatar(one.getAvatar()).setCount(Math.toIntExact(lambdaQuery().eq(CourseAction::getTargetId, action.getId()).eq(CourseAction::getType, ActionType.Reply).count()));

        }).collect(Collectors.toList());
        return R.success(collect);
    }


    /**
     * 获取互动者基础信息
     *
     * @param userId 用户 ID
     * @return {@link User }
     */
    private User getActionUser(Integer userId) {
        User one = userService.lambdaQuery().select(User::getUsername, User::getNickname, User::getAvatar).eq(User::getId, userId).one();
        if (one == null) {
            one = new User().setUsername("已注销").setNickname("已注销").setAvatar("");
        }
        return one;
    }

    /**
     * 编辑课程问题
     *
     * @param issueId 问题 ID
     * @param content 内容
     * @return {@link R }<{@link CourseAction }>
     */
    @Override
    public R<CourseAction> updateIssue(Integer issueId, String content) {
        return null;
    }

    /**
     * 删除问题
     *
     * @param issueId 问题 ID
     * @return {@link R }<{@link Boolean }>
     */
    @Override
    public R<Boolean> deleteIssue(Integer issueId) {
        return null;
    }

    @Override
    public Long CountIssue(Integer user_id) {
        QueryWrapper<CourseAction> qe = new QueryWrapper<>();
        qe.eq("type", "Issue");
        qe.eq("user_id", user_id);
        return courseActionDao.selectCount(qe);
    }

    /**
     * 添加回复
     *
     * @param courseId 课程 ID
     * @param issueId  问题 ID
     * @param content  内容
     * @return {@link R }<{@link CourseReply }>
     */
    @Override
    public R<CourseReply> addReply(Integer courseId, Integer issueId, String content) {
        sensitiveService.checkSensitive(content);
        final CourseAction reply = new CourseAction(ActionType.Reply).setTime(new Date()).setUserId(SaTokenUtil.getLoginId()).setCourseId(courseId).setTargetId(issueId).setContent(content);
        final boolean save = save(reply);
        if (!save) throw new AppException("回复失败");
        final User actionUser = getActionUser(reply.getUserId());
        final CourseReply courseReply = new CourseReply().setId(reply.getId()).setContent(content).setTime(reply.getTime()).setUserId(reply.getUserId()).setAvatar(actionUser.getAvatar()).setUsername(actionUser.getUsername()).setNickname(actionUser.getNickname());
        return R.success(courseReply).msg("回复成功");
    }

    /**
     * 查询 issueForm 的回复列表
     *
     * @param issueId 问题 ID
     * @return {@link R }<{@link List }<{@link CourseReply }>>
     */
    @Override
    public R<List<CourseReply>> getReplies(Integer issueId) {
        final List<CourseReply> replies = lambdaQuery().eq(CourseAction::getType, ActionType.Reply).eq(CourseAction::getTargetId, issueId).list().stream().map(action -> {
            final User actionUser = getActionUser(action.getUserId());
            return new CourseReply().setId(action.getId()).setContent(action.getContent()).setTime(action.getTime()).setUserId(action.getUserId()).setUsername(actionUser.getUsername()).setNickname(actionUser.getNickname()).setAvatar(actionUser.getAvatar());
        }).toList();
        return R.success(replies);
    }

    /**
     * 删除回复
     *
     * @param replyId 回复 ID
     * @return {@link R }<{@link Boolean }>
     */
    @Override
    public R<Boolean> deleteReply(Integer replyId) {
        return null;
    }

    @Override
    public Long CountReply(Integer user_id) {
        QueryWrapper<CourseAction> qe = new QueryWrapper<>();
        qe.eq("type", "Reply");
        qe.eq("user_id", user_id);
        return courseActionDao.selectCount(qe);
    }
}
