package com.wenlidao.service.impl;

import cn.hutool.captcha.AbstractCaptcha;
import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.core.util.RandomUtil;
import com.wenlidao.common.api.R;
import com.wenlidao.common.exception.AppException;
import com.wenlidao.common.util.EmailUtil;
import com.wenlidao.common.vo.ImageCaptcha;
import com.wenlidao.service.CaptchaService;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 验证码业务实现类
 * @author INYXIN
 * @version 1.0.0
 */
@Slf4j
@Service
public class CaptchaServiceImpl implements CaptchaService {

    @Resource
    @Qualifier ("captchaAppDao")
    CaptchaDao captchaDao;
    @Resource
    EmailUtil emailUtil;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public R<ImageCaptcha> createImageCaptcha() {
        //调用Hutool工具包生成图片验证码
        final AbstractCaptcha circleCaptcha = CaptchaUtil.createLineCaptcha(230, 100, 4, 30);

        final String code = circleCaptcha.getCode();
        //将验证码存入redis  180秒
        final int seconds = 60 * 3;
        final String base64 = circleCaptcha.getImageBase64Data();
        final String uuid = UUID.randomUUID().toString().replace("-", "");
        final String expire = simpleDateFormat.format(new Date(System.currentTimeMillis() + seconds * 1000));
        captchaDao.saveCaptcha(uuid, code, seconds);
        final ImageCaptcha imageCaptcha = new ImageCaptcha(uuid, base64, expire);
        log.debug("生成的图片验证码为：{} -> {}, 有效期:{}", code, uuid, expire);
        //返回结果
        return R.success(imageCaptcha);
    }

    @Override
    public R<Object> createEmailCaptcha(String email) {
        log.debug("发送邮件验证码开始...");
        //创建随机4位验证码
        String code = RandomUtil.randomNumbers(4);//数字验证码
        //发送邮件
        emailUtil.sentEmail("验证码", "您正在获取邮箱验证码,验证码为 : " + code, email);
        final int seconds = 60 * 5;
        //将验证码存于缓存中
        captchaDao.saveCaptcha(email, code, seconds);
        String expire = simpleDateFormat.format(new Date(System.currentTimeMillis() + seconds * 1000));
        log.info("发送邮箱验证码:{} -> {}  有效期:{}", code, email, expire);
        return R.success(null).msg("邮件发送成功-> " + email + "，请注意查收");
    }


    @Override
    public void verifyCaptcha(String key, String captcha) {

        final String value = captchaDao.getCaptchaByKey(key);
        if (!StringUtils.hasText(value)) {
            throw new AppException("验证码失效");
        }
        if (!value.equalsIgnoreCase(captcha)) {
            throw new AppException("验证码错误");
        }
        //校验通过删除验证码
        captchaDao.removeCaptchaById(key);

    }


    /**
     * 使用Redis才存储验证码
     * @author INYXIN
     * @version 1.0.0
     * 3月 5, 2024   16:58
     */
    @Repository ("captchaRedisDao")
    @Primary
    static class CaptchaRedisDao implements CaptchaDao {
        @Resource
        StringRedisTemplate stringRedisTemplate;

        @Override
        public void saveCaptcha(String key, String value, int seconds) {
            stringRedisTemplate.opsForValue().set(CAPTCHA_KEY_PREFIX + key, value, seconds, TimeUnit.SECONDS);
        }

        @Override
        public String getCaptchaByKey(String uuid) {
            return stringRedisTemplate.opsForValue().get(CAPTCHA_KEY_PREFIX + uuid);
        }

        @Override
        public Boolean removeCaptchaById(String key) {
            return stringRedisTemplate.delete(CAPTCHA_KEY_PREFIX + key);
        }

    }
    @Repository ("captchaAppDao")
    static
    class CaptchaAppDao implements CaptchaDao {
        //使用线程安全的ConcurrentHashMap
        private final ConcurrentHashMap<String, ValueWithExpiry> map = new ConcurrentHashMap<>();

        public CaptchaAppDao() {
            // 定期清理过期的验证码,30秒
            ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
            executorService.scheduleAtFixedRate(() -> {
                long currentTimeMillis = System.currentTimeMillis();
                map.entrySet().removeIf(entry -> currentTimeMillis > entry.getValue().expiryTime);
            }, 30, 30, TimeUnit.SECONDS);
        }

        @Override
        public void saveCaptcha(String key, String value, int seconds) {
            long expiryTime = System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(seconds);
            map.put(key, new ValueWithExpiry(value, expiryTime));
        }

        @Override
        public String getCaptchaByKey(String key) {
            ValueWithExpiry valueWithExpiry = map.get(key);
            if (valueWithExpiry != null && System.currentTimeMillis() <= valueWithExpiry.expiryTime) {
                return valueWithExpiry.value;
            }
            return null;
        }

        @Override
        public Boolean removeCaptchaById(String key) {
            return map.remove(key) != null;
        }

        /**
         * 值与到期
         * @author INYXIN
         * @version 1.0.0
         * 3月 5, 2024   18:09
         */
        private record ValueWithExpiry(String value, long expiryTime) {
        }
    }
}
