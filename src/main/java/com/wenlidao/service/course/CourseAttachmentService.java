package com.wenlidao.service.course;

import com.wenlidao.common.api.R;
import com.wenlidao.common.dto.CourseTableQueryDTO;
import com.wenlidao.entity.course.Course;
import com.wenlidao.entity.course.CourseAttachment;
import com.wenlidao.entity.enumerate.SysFileCategory;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 课程附件业务类
 * @author INYXIN
 * @version 1.0.0
 * 4月 6, 2024   20:11
 */

public interface CourseAttachmentService {

    /**
     * 列出课程附件
     * @param courseId 课程 ID
     * @return {@link R }<{@link List }<{@link CourseAttachment }>>
     */
    R<List<CourseAttachment>> listCourseAttachment(Integer courseId);

    /**
     * 添加已上传的文件为附件 (直接上传)
     * @param courseId      课程 ID
     * @param courseItemId  课程项目 ID
     * @param category      分类
     * @param multipartFile 附件
     * @return {@link R }<{@link CourseAttachment }>
     */
    R<CourseAttachment> addAttachment(Integer courseId, Integer courseItemId, SysFileCategory category, MultipartFile multipartFile);

    /**
     * 添加已上传的文件为附件 (间接上传)
     * @param courseId     课程 ID
     * @param courseItemId 课程项目 ID
     * @param fileId       文件 ID
     * @return {@link R }<{@link CourseAttachment }>
     */
    R<CourseAttachment> addAttachment(Integer courseId,Integer courseItemId,Integer fileId);

    R<List<Course>> selectAll(CourseTableQueryDTO queryDTO);

    R<Boolean> delete(int[] ids);
}
