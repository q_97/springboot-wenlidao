package com.wenlidao.service.course.action;

import com.wenlidao.common.api.R;
import com.wenlidao.common.vo.course.CourseIssue;
import com.wenlidao.entity.course.CourseAction;

import java.util.List;

/**
 * 课程问题发布业务类
 * @author INYXIN
 * @version 1.0.0
 * 3月 29, 2024   23:04
 */
public interface IssueService {
    /**
     * 添加课程问题
     * @param courseId 课程 ID
     * @param content  内容
     * @return {@link R }<{@link CourseIssue }>
     */
    R<CourseIssue> addIssue(Integer courseId,String content);

    /**
     * 查询课程的问题
     * @param courseId 课程 ID
     * @return {@link R }<{@link List }<{@link CourseIssue }>>
     */
    R<List<CourseIssue>> getIssues(Integer courseId);

    /**
     * 编辑课程问题
     * @param issueId 问题 ID
     * @param content 内容
     * @return {@link R }<{@link CourseAction }>
     */
    R<CourseAction> updateIssue(Integer issueId, String content);

    /**
     * 删除问题
     * @param issueId 问题 ID
     * @return {@link R }<{@link Boolean }>
     */
    R<Boolean> deleteIssue(Integer issueId);

    Long CountIssue(Integer user_id);
}
