package com.wenlidao.service.course.action;

import com.wenlidao.common.api.R;
import com.wenlidao.common.vo.course.CourseReply;
import com.wenlidao.entity.course.CourseAction;

import java.util.List;

public interface ReplyService {

    /**
     * 添加回复
     * @param courseId 课程 ID
     * @param issueId  问题 ID
     * @param content  内容
     * @return {@link R }<{@link CourseReply }>
     */

    R<CourseReply> addReply(Integer courseId, Integer issueId, String content);


    /**
     * 查询 issueForm 的回复列表
     * @param issueId 问题 ID
     * @return {@link R }<{@link List }<{@link CourseReply }>>
     */
    R<List<CourseReply>> getReplies(Integer issueId);

    /**
     * 删除回复
     * @param replyId 回复 ID
     * @return {@link R }<{@link Boolean }>
     */
    R<Boolean> deleteReply(Integer replyId);

    Long CountReply(Integer user_id);

}
