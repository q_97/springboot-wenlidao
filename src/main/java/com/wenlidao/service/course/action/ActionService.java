package com.wenlidao.service.course.action;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wenlidao.common.api.R;
import com.wenlidao.entity.course.CourseAction;
import com.wenlidao.entity.enumerate.ActionType;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * 课程互动业务类
 * @author INYXIN
 * @version 1.0.0
 * 3月 29, 2024   16:39
 */
@Transactional
public interface ActionService extends IService<CourseAction>
        , IssueService
        , ReplyService
        , AnnouncementService
        , SelectedCourseService {
    /**
     * 更新课程进度进度
     * @param courseId 课程 ID
     * @param itemId   课程项 ID
     * @return {@link R }<{@link Void }>
     */
    R<Void> updateProgress(Integer courseId, Integer itemId);

    /**
     * 获取进度
     * @param courseId 课程 ID
     * @return {@link R }<{@link CourseAction }>
     */
    CourseAction getProgress(Integer courseId);

    /**
     * 记录浏览行为
     * @param courseId 课程 ID
     * @return {@link R }<{@link Void }>
     */
    R<Void> recordView(Integer courseId);

    /**
     * 统计课程互动元数据
     * @param courseId 课程 ID
     * @return {@link R }<{@link Map }<{@link ActionType }, {@link Long }>>
     */
    R<Map<ActionType, Long>> getCourseActionMetaData(Integer courseId);

    Long CountView(Integer user_id);
}
