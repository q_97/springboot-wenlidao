package com.wenlidao.service.course.action;

import com.wenlidao.common.api.R;
import com.wenlidao.entity.course.CourseAction;

import java.util.List;

/**
 * 课程公告(通知)业务类
 * @author INYXIN
 * @version 1.0.0
 * 3月 29, 2024   16:50
 */
public interface AnnouncementService {

    /**
     * 添加公告
     * @param courseId
     * @param content  内容
     * @return {@link R }<{@link CourseAction }>
     */
    R<CourseAction> addAnnouncement(Integer courseId, String content);

    /**
     * 获取课程公告列表
     * @param courseId 课程 ID
     * @return {@link R }<{@link List }<{@link CourseAction }>>
     */
    R<List<CourseAction>> getAnnouncements(Integer courseId);

    /**
     * 获取我的公告通知
     * @return {@link R }<{@link List }<{@link CourseAction }>>
     */
    R<List<CourseAction>> getNoticeAnnouncements();

    /**
     * 更新课程公告
     * @param noticeId 通知 ID
     * @param courseId
     * @param content  内容
     * @return {@link R }<{@link CourseAction }>
     */
    R<CourseAction> updateAnnouncement(Integer noticeId, Integer courseId, String content);

    /**
     * 删除课程公告
     * @param id 同上
     * @return {@link R }<{@link Boolean }>
     */
    R<Boolean> deleteAnnouncement(Integer id);
}
