package com.wenlidao.service.course.action;

import com.wenlidao.common.api.R;
import com.wenlidao.entity.course.Course;

import java.util.List;


/**
 * 学生选课业务类
 * @author INYXIN
 * @version 1.0.0
 * 3月 29, 2024   16:50
 */
public interface SelectedCourseService {
    /**
     * 选择课程
     * @param courseId 课程 ID
     * @return {@link R }<{@link Boolean }>
     */
    R<Void> selectedCourse(Integer courseId);
    /**
     * 取消选课
     * @param courseId 课程 ID
     * @return {@link R }<{@link Boolean }>
     */
    R<Void> cancelSelected(Integer courseId);

    /**
     * 查询当前用户已选课程
     * @return {@link R }<{@link List }<{@link Course }>>
     */
    R<List<Course>> listSelectedCourse();

    Long CountSelect(Integer user_id);
}
