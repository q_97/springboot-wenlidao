package com.wenlidao.service.course;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wenlidao.common.api.R;
import com.wenlidao.entity.course.Course;
import com.wenlidao.entity.course.CourseItem;
import com.wenlidao.entity.enumerate.DictEnum;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 课程项目服务
 * @author INYXIN
 * @version 1.0.0
 * 4月 6, 2024   20:46
 */
@Transactional
public interface CourseItemService extends IService<CourseItem> {
    /**
     * 课程项下拉选择框数据
     * @param courseId 课程 ID
     * @return {@link R }<{@link List }<{@link DictEnum }>>
     */
    R<List<DictEnum>> selectionCourseItem(Integer courseId);

    /**
     * 获取课程和项目
     * @param courseId 课程 ID
     * @return {@link R }<{@link Course }>
     */
    R<Course> getCourseAndItems(Integer courseId);
}
