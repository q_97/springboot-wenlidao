package com.wenlidao.service.course;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wenlidao.entity.course.Course;
import org.springframework.transaction.annotation.Transactional;


/**
 * 课程业务类
 * @author INYXIN
 * @version 1.0.0
 * 3月 29, 2024   16:40
 */
@Transactional
public interface CourseService extends IService<Course>,
        CourseSearchService,
        CoursePostService,
        CourseAttachmentService {

}
