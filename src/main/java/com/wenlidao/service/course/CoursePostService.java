package com.wenlidao.service.course;

import com.wenlidao.common.api.R;
import com.wenlidao.entity.course.Course;
import org.springframework.transaction.annotation.Transactional;

/**
 * 课程操作型业务类
 * @author INYXIN
 * @version 1.0.0
 * 3月 29, 2024   22:57
 */
@Transactional
public interface CoursePostService {
    /**
     * 添加课程
     * @param course 课程
     * @return {@link R }<{@link Course }>
     */
    R<Course> addCourse(Course course);

    /**
     * 更新课程
     * @param course 课程
     * @return {@link R }<{@link Course }>
     */
    R<Course> updateCourse(Course course);

    /**
     * 删除课程
     * @param id 同上
     * @return {@link R }<{@link Boolean }>
     */
    R<Boolean> deleteCourse(Integer id);

}
