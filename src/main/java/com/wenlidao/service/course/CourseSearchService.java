package com.wenlidao.service.course;

import com.wenlidao.common.api.R;
import com.wenlidao.entity.course.Course;
import com.wenlidao.entity.enumerate.CourseCategory;
import com.wenlidao.entity.enumerate.CourseSchoolLevel;
import com.wenlidao.entity.enumerate.CourseSubject;
import com.wenlidao.entity.enumerate.CourseType;

import java.util.List;
import java.util.Map;

/**
 * 课程查询业务类
 * @author INYXIN
 * @version 1.0.0
 * 3月 29, 2024   22:57
 */
public interface CourseSearchService {
    /**
     * 根据课程ID查询课程
     * @param id 课程ID
     * @return {@link R }<{@link Course }>
     */
    R<Course> getCourse(Integer id);
    /**
     * 建立索引
     */

    void Indexing();

    R<List<Map<String, String>>> homeSearch(String search, String category, String type, String subject, String school ,String sort, Integer page, Integer size);

    Double getUserActivity(Integer user_id,Long course_id);

    R<List<Map<String, String>>> getRecommendCourseByItems(Long user_id);

    R<List<Map<String, String>>> display(String obj,String objVal, Integer size);

    R<List<Course>> getPublish();

}
