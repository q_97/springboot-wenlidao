package com.wenlidao;

import cn.dev33.satoken.SaManager;
import com.wenlidao.common.config.SystemConstants;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class WenlidaoApplication {
    public static void main(String[] args) {
        final ConfigurableApplicationContext context = SpringApplication.run(WenlidaoApplication.class, args);
        after(context);
    }

    private static void after(ConfigurableApplicationContext context) {
        final SystemConstants constants = context.getBean(SystemConstants.class);
        System.err.printf("""
                        ============[启动成功]=============
                        Sa-token 配置 :%s
                        SpringDoc 文档:http://%s:%s/doc.html
                        Vue : http://%s:%s/
                        =================================
                        """, SaManager.getConfig().toString(),
                constants.SERVER_IP, constants.SERVER_PORT,
                constants.FRONT_IP, constants.FRONT_PORT
        );
    }
}
