package com.wenlidao.entity.model;

public interface Recommend {

    long getUserId();
    long getItemId();
    float getValue();
}

