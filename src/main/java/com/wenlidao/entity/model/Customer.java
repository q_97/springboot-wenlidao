package com.wenlidao.entity.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * 计算对象的实体类
 * @author jfcjjcddmd
 * @version 1.0.0
 * 4月 25, 2024   19:23
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Customer implements Recommend {

    // 数据id
    private Long id;
    // 用户id
    private Long customerId;
    // 商品id
    private Long commodityId;
    // 分值
    private Float value;

    // private Date createTime;

    @Override
    public long getUserId() {
        return this.customerId;
    }
    @Override
    public long getItemId() {
        return this.commodityId;
    }
    @Override
    public float getValue() {
        return this.value;
    }
}

