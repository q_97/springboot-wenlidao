package com.wenlidao.entity.model;

import lombok.Data;

/**
 * 功能：统计用户行为数据
 * 作者：jfcjjcddmd
 * 日期：2024/4/26 10:45
 */
@Data
public class UserActivity {
    private int viewCount;
    private int selectedCourses;
    private int questionsAsked;
    private int answersProvided;


}
