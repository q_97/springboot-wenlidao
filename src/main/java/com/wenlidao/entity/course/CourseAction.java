package com.wenlidao.entity.course;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wenlidao.entity.enumerate.ActionType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

@Data
@Accessors (chain = true)
@Schema (description = "课程互动实体类")
@NoArgsConstructor
@TableName ("course_action")
public class CourseAction {
    @Schema (description = "互动id")
    @TableId
    Integer id;
    @Schema (description = "用户id")
    Integer userId;
    @Schema (description = "课程id")
    Integer courseId;
    @Schema (description = "互动类型")
    ActionType type;
    @Schema (description = "互动时间")
    Date time;
    @Schema (description = "互动内容")
    String content;
    @Schema (description = "互动目标id")
    Integer targetId;
    @TableField (exist = false)
    List<CourseAction> reply;

    public CourseAction(ActionType type) {
        this.type = type;
    }

}
