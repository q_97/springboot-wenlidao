package com.wenlidao.entity.course;

import com.wenlidao.entity.enumerate.DictEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 课程详情类
 * @author INYXIN
 * @version 1.0.0
 * 3月 29, 2024   15:50
 */
@Data
@Accessors(chain = true)
@Schema (description = "课程项")
public class CourseItem implements DictEnum {
    @Schema (description = "ID")
    Integer id;
    @Schema (description = "所属课程ID")
    Integer courseId;
    @Schema (description = "课程名")
    String name;
    @Schema (description = "资源视频URL地址")
    String video;
    @Schema (description = "发布时间")
    String time;
    @Schema (description = "时长")
    String duration;

    /**
     * 字典标签(前台显示的数据),Jackson序列化时输出toString(),需要在重写toString()
     * @return {@link String }
     */
    @Override
    public String getLabel() {
        return name;
    }

    @Override
    public String name() {
        return String.valueOf(id);
    }
}
