package com.wenlidao.entity.course;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.wenlidao.entity.SysFile;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 课程附件
 * @author INYXIN
 * @version 1.0.0
 * 3月 29, 2024   22:51
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode (callSuper = true)
@Schema (description = "课程附件")
@TableName ("course_attachment")
public class CourseAttachment extends Model<CourseAttachment> {
    @TableId
    @Schema (description = "附件ID")
    Integer id;
    @Schema (description = "课程 ID")
    Integer courseId;
    @Schema (description = "文件 ID")
    Integer fileId;
    @Schema (description = "关联课程项ID")
    Integer courseItemId;
    @TableField(exist = false)
    SysFile file;
    @TableField(exist = false)
    CourseItem courseItem;
}
