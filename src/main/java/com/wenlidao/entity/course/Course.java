package com.wenlidao.entity.course;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.github.dreamyoung.mprelation.JoinColumn;
import com.github.dreamyoung.mprelation.Lazy;
import com.github.dreamyoung.mprelation.OneToMany;
import com.github.dreamyoung.mprelation.OneToOne;
import com.wenlidao.common.util.lucene.IndexField;
import com.wenlidao.entity.SysFile;
import com.wenlidao.entity.User;
import com.wenlidao.entity.enumerate.CourseCategory;
import com.wenlidao.entity.enumerate.CourseSchoolLevel;
import com.wenlidao.entity.enumerate.CourseSubject;
import com.wenlidao.entity.enumerate.CourseType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * 课程实体类
 * @author INYXIN
 * @version 1.0.0
 * 3月 29, 2024   15:40
 */
@Schema (description = "课程实体类")
@TableName ("course")
@Accessors (chain = true)
@Data
public class Course {
    @Schema (description = "课程ID")
    @TableId
    @IndexField (tokenized = false)
    Integer id;
    @Schema (description = "教师ID")
    @IndexField (indexed = true,tokenized = true,stored = true)
    Integer teacherId;
    @Schema (description = "课程名")
    @IndexField (indexed = true,tokenized = true,stored = true)
    String name;
    @Schema (description = "课程封面")
    @IndexField (indexed = false, stored = true)
    String cover;
    @Schema (description = "课程描述")
    @IndexField (indexed = true,tokenized = true,stored = true)
    String description;
    @IndexField (indexed = true,tokenized = false,stored = true)
    @Schema (description = "课时")
    Integer hour;
    @Schema (description = "课程分类")
    @IndexField (indexed = true,tokenized = false,stored = true)
    CourseCategory category;
    @Schema (description = "课程类型")
    @IndexField (indexed = true,tokenized = false,stored = true)
    CourseType type;
    @Schema (description = "课程学科")
    @IndexField (indexed = true,tokenized = false,stored = true)
    CourseSubject subject;
    @Schema (description = "课程等级")
    @IndexField (indexed = true,tokenized = false,stored = true)
    CourseSchoolLevel school;
    @IndexField (indexed = true,tokenized = false,stored = true)
    @Schema (description = "发布时间")
    Date time;
    @Schema (description = "教师")
    @TableField (exist = false)
    User teacher;
    @Schema (description = "教师名字")
    @IndexField (indexed = true,tokenized = false,stored = true)
    @TableField (exist = false)
    String teacherName;
    @Schema (description = "课程视频项")
    @TableField (exist = false)
    List<CourseItem> items;
    @Schema (description = "课程资源附件")
    @TableField (exist = false)
    List<SysFile> attachments;
}
