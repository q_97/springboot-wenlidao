package com.wenlidao.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.wenlidao.entity.enumerate.Gender;
import com.wenlidao.entity.enumerate.Role;
import jakarta.validation.constraints.Pattern;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * 用户
 * @author INYXIN
 * @version 1.0.0
 * 3月 17, 2024   22:49
 */
@Data
@Accessors(chain = true)
@TableName (value = "sys_user", autoResultMap = true)
public class User implements Serializable {
    /**
     * 编号
     */
    @TableId
    Integer id;
    /**
     * 用户名
     */
    String username;
    /**
     * 昵称
     */
    String nickname;
    /**
     * 密码
     */
    String password;
    /**
     * 性别
     */
    Gender gender;
    /**
     * 角色
     */
    Role role;
    /**
     * 位置
     */
    String location;
    /**
     * 描述
     */
    String remark;
    /**
     * 头像
     */
    String avatar;
    @DateTimeFormat(pattern = "yyyy-MM-dd" )
    /**
     * 生日
     */
    Date birthday;
    /**
     * 状态
     */
    boolean disabled;
    /**
     * 账户列表
     */
    @TableField (typeHandler = JacksonTypeHandler.class)
    HashMap<String, String> accounts;
}
