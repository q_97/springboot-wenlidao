package com.wenlidao.entity.enumerate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * 课程学校水平
 * @author INYXIN
 * @version 1.0.0
 * 3月 27, 2024   23:32
 */
@AllArgsConstructor
@Getter
@Schema (type = "string", allowableValues = {"ALL", "S_985_211", "S_985", "S_211", "S_Undergraduate", "S_Junior", "Other"})
@JsonFormat (shape = Shape.OBJECT)
public enum CourseSchoolLevel implements DictEnum {
    ALL("全部"),
    S_985_211("双一流"),
    S_985("985"),
    S_211("211"),
    S_Undergraduate("普通本科"),
    S_Junior("高职高专"),
    Other("其他");
    /**
     * 学校
     */
    final String label;
}
