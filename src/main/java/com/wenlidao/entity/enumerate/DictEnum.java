package com.wenlidao.entity.enumerate;

import com.baomidou.mybatisplus.annotation.IEnum;

/**
 * 字典
 * <pre>
 *     字典枚举类系统:
 *          1. 字典枚举类必须实现{@link DictEnum}接口
 *          2. 枚举类必须包含一个{@link #getLabel()} 和{@link #toString()}方法,用于返回字典标签(前台显示的数据)
 *          3. 枚举类必须包含一个{@link #getValue()}方法,用于返回字典值(数据库存储值)
 *      原理:
 *         在接收参数时, 需要完整的枚举名才能接收到,否则抛出参数接收异常
 *         在返回数据时,弃用了@JsonValue注解,使用{@link #getLabel()}+{@link toString()}方法,对枚举进行数据翻译,返回字典标签(前台显示的数据)
 *         在保存数据库时,重写了MybatisPlus IEnum类的getValue()方法,使得数据库只存储code值
 *
 * </pre>
 * @author INYXIN
 * @version 1.0.0
 * 3月 15, 2024   17:51
 */

public interface DictEnum extends IEnum<String> {
    /**
     * 字典标签(前台显示的数据),Jackson序列化时输出toString(),需要在重写toString()
     * @return {@link String }
     */
    String getLabel();


    /**
     * 枚举数据库存储值,mybatis-plus 存数据库时使用此值
     */
    @Override
    default String getValue() {        //默认是枚举名
        return this.name();
    }

    String name();
}