package com.wenlidao.entity.enumerate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 课程排序方案
 * @author INYXIN
 * @version 1.0.0
 * 3月 27, 2024   23:31
 */
@AllArgsConstructor
@Getter
@Schema (type = "string", allowableValues = {"Synthesis", "Latest"})
@JsonFormat (shape = Shape.OBJECT)
public enum CourseSort implements DictEnum {
    Synthesis("综合排序"),
    Latest("最新发布");
    final String label;
}
