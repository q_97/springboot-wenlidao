package com.wenlidao.entity.enumerate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 课程互动类型
 *
 * @author INYXIN
 * @version 1.0.0
 * 4月 2, 2024   08:16
 */
@AllArgsConstructor
@Getter
@Schema(type = "string", allowableValues = {"View", "Select", "Progress", "Notice", "Issue", "Reply", "Put"})
@JsonFormat(shape = Shape.OBJECT)
public enum ActionType implements DictEnum {
    View("浏览"),
    Select("选课"),
    Progress("课程进度"),
    Notice("课程公告"),
    Issue("课程提问"),
    Reply("课程回答"),
    Put("发布课程");
    final String label;
}
