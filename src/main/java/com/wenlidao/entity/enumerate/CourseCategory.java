package com.wenlidao.entity.enumerate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 课程类别
 * @author INYXIN
 * @version 1.0.0
 * 3月 27, 2024   23:31
 */
@Getter
@AllArgsConstructor
@Schema (type = "string", allowableValues = {"All", "GoldLesson", "Good", "Pay"})
@JsonFormat(shape = Shape.OBJECT)
public enum CourseCategory implements DictEnum {
    All("全部"),
    GoldLesson("金牌课程"),
    Good("精品课程"),
    Pay("名人精选");

    final String label;
}

