package com.wenlidao.entity.enumerate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 用户的性别
 * @author INYXIN
 * @version 1.0.0
 */
@AllArgsConstructor
@Getter
@JsonFormat (shape = Shape.OBJECT)
public enum Gender implements DictEnum {
    MALE("MALE", "男性"),
    FEMALE("FEMALE", "女性"),
    UNKNOWN("UNKNOWN", "未知");
    private final String value;
    private final String label;
}
