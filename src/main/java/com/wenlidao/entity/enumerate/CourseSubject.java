package com.wenlidao.entity.enumerate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 课程学科
 *
 * @author INYXIN
 * @version 1.0.0
 * 3月 27, 2024   23:32
 */
@AllArgsConstructor
@Getter
@Schema(type = "string", allowableValues = {"ALL", "Philosophy", "Economics", "Jurisprudence", "Pedagogy", "literature", "History", "Science", "Engineering", "Agriculture", "Medicine", "Management", "Art", "Other"})
@JsonFormat(shape = Shape.OBJECT)
public enum CourseSubject implements DictEnum {
    ALL("全部"),
    Philosophy("哲学"),
    Economics("经济学"),
    Jurisprudence("法学"),
    Pedagogy("教育学"),
    literature("文学"),
    History("历史学"),
    Science("理学"),
    Engineering("工学"),
    Agriculture("农学"),
    Medicine("医学"),
    Management("管理学"),
    Art("艺术学"),
    Other("交叉学科");
    final String label;
}