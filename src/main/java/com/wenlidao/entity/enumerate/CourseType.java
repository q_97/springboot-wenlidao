package com.wenlidao.entity.enumerate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 课程类型
 * @author INYXIN
 * @version 1.0.0
 * 3月 27, 2024   23:32
 */
@AllArgsConstructor
@Getter
@Schema (type = "string", allowableValues = {"All", "General", "Major", "Public"})
@JsonFormat (shape = Shape.OBJECT)
public enum CourseType implements DictEnum {
    All("全部"),
    General("通识课"),
    Major("专业课"),
    Public("公共必修课");
    final String label;
}
