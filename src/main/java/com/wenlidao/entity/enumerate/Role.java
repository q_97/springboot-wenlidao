package com.wenlidao.entity.enumerate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
@Schema (type = "string", allowableValues = {"Admin", "Teacher", "User"})
@JsonFormat (shape = Shape.OBJECT)
public enum Role implements DictEnum {
    Admin("管理员"),
    Teacher("教师"),
    User("用户");
    public final String label;
}
