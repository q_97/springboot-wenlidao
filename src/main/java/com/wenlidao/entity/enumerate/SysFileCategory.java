package com.wenlidao.entity.enumerate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.wenlidao.common.exception.AppException;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 系统文件分类
 * @author INYXIN
 * @version 1.0.0
 * 3月 28, 2024   17:20
 */
@Getter
@JsonFormat (shape = Shape.OBJECT)
@Schema(type = "string", allowableValues =  {"Default", "Avatar", "Cover", "Image", "Video", "Audio", "Document", "Zip", "Other"})
public enum SysFileCategory implements DictEnum {
    Default( "默认分类"),
    Avatar( "用户头像"),
    Cover( "封面"),
    Image( "图片"),
    Video( "视频"),
    Audio("音频"),
    Document("文档"),
    Zip("压缩文件"),
    //未来扩展文件分类
    Other("其他");
    final String label;

    SysFileCategory(String label) {
        this.label = label;
    }

    /**
     * 根据Value找到分类
     * @param category 类别
     * @return {@link SysFileCategory }
     */
    public static SysFileCategory find(String category) {
        for (SysFileCategory sysFileCategory : values()) {
            if (sysFileCategory.getValue().equals(category)) {
                return sysFileCategory;
            }
        }
        throw new AppException("不支持的文件分类:" + category);
    }
}
