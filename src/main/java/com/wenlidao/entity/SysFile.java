package com.wenlidao.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wenlidao.entity.enumerate.SysFileCategory;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

/**
 * 系统应用文件
 * <pre>
 *    系统中产生的文件,(如用户上传的文件,)
 *    定义: 文件访问规则
 *    在上传文件后会为该文件分配一个 唯一hash值
 *    访问文件: http://localhost:8080/file/hash 即可访问
 *    由服务器转发到正确的文件地址 如 D:/root/category/hash.jpg
 * </pre>
 * @author INYXIN
 * @version 1.0.0
 * 2月 7, 2024   00:08
 */
@Schema (description = "系统应用文件")
@Data
@TableName ("sys_file")
public class SysFile implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    @TableId
    @Schema (description = "文件ID")
    private Integer id;
    @Schema (description = "文件拥有者ID")
    private Integer userId;
    @Schema (description = "文件MD5散列值")
    private String hash;
    @Schema (description = "文件名")
    private String filename;
    @Schema (description = "类别")
    private SysFileCategory category;
    @Schema (description = "MIME 类型")
    private String mimeType;
    @Schema (description = "文件大小,单位:字节")
    private Long size;
    @Schema (description = "上传时间")
    private Date time;
    @Schema (description = "上传用户")
    @TableField(exist = false)
    private User owner;
}
