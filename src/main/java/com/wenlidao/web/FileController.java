package com.wenlidao.web;

import cn.dev33.satoken.annotation.SaCheckLogin;
import com.wenlidao.common.api.R;
import com.wenlidao.entity.SysFile;
import com.wenlidao.entity.enumerate.SysFileCategory;
import com.wenlidao.service.ISysFileService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.enums.ParameterStyle;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import static com.wenlidao.service.impl.SysFileServiceImpl.FILE_ACCESS_CONTEXT;

@Tag (name = "文件API")
@RestController
@RequestMapping (FILE_ACCESS_CONTEXT)
public class FileController {
    @Resource
    ISysFileService fileService;

    @Parameters ( {
            @Parameter (name = "hash", description = "通过文件hash值访问文件", in = ParameterIn.PATH, required = true),
            @Parameter (name = "download", description = "是否下载文件", in = ParameterIn.QUERY)
    })
    @Operation (summary = "访问文件")
    @GetMapping ("{hash}")
    public ResponseEntity<UrlResource> accessFile(@PathVariable ("hash") String hash, @RequestParam (required = false, defaultValue = "false") boolean download) {
        return fileService.accessFile(hash, download);
    }

    @Parameters ( {
            @Parameter (name = "file", description = "二进制文件", in = ParameterIn.QUERY, required = true, schema = @Schema (type = "string", format = "binary")),
            @Parameter (name = "category", description = "文件分类", in = ParameterIn.QUERY, required = true)
    })
    @Parameter (style = ParameterStyle.FORM)
    @Operation (summary = "上传文件", description = "")
    @PostMapping ("upload")
    public R<String> uploadFile(@RequestParam ("file") MultipartFile file, @RequestParam ("category") SysFileCategory category) {
        final SysFile sysFile = fileService.upload(file, category);
        return R.success(fileService.getAccessSrc(sysFile)).msg("文件上传成功");
    }

    @Parameter (name = "hash", description = "文件hash值", in = ParameterIn.PATH, required = true)
    @Operation (summary = "删除文件", description = "")
    @DeleteMapping ("delete/{hash}")
    public R<Boolean> delete(@PathVariable ("hash") String hash) {
        return fileService.deleteFile(hash);
    }

    @Operation (summary = "获取我的文件", description = "")
    @SaCheckLogin
    @GetMapping ("/my")
    public R<List<SysFile>> myFiles() {
        return fileService.myFiles();
    }

    @Operation (summary = "获取文件分类", description = "")
    @GetMapping ("category")
    public R<SysFileCategory[]> category() {
        return R.success(SysFileCategory.values());
    }
}
