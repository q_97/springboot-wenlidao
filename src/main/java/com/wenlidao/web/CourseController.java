package com.wenlidao.web;

import com.wenlidao.common.api.R;
import com.wenlidao.common.dto.CourseTableQueryDTO;
import com.wenlidao.entity.course.Course;
import com.wenlidao.entity.course.CourseAttachment;
import com.wenlidao.entity.enumerate.*;
import com.wenlidao.service.course.CourseAttachmentService;
import com.wenlidao.service.course.CourseService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.enums.ParameterStyle;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 课程API
 * @author INYXIN
 * @version 1.0.0
 * 4月 6, 2024   20:23
 */
@Tag(name = "课程API 4月 6, 2024   20:23", description = "课程API 4月 6, 2024   20:23")
@RequestMapping("/course")
@RestController
public class CourseController {
    @Resource
    CourseService courseService;
    @Resource
    CourseAttachmentService attachmentService;

    /**
     * 获取课程信息
     * @param courseId 课程 ID
     * @return {@link R }<{@link Course }>
     */
    @Parameter(name = "courseId", description = "课程 ID", in = ParameterIn.PATH, required = true)
    @Operation(summary = "获取课程信息", description = "")
    @GetMapping ("{courseId}")
    public R<Course> getCourse(@PathVariable ("courseId") Integer courseId) {
        return courseService.getCourse(courseId);
    }

    /**
     * 发布课程
     * @return
     */
    @GetMapping ("/publish")
    public R<List<Course>> getPublish() {
        return courseService.getPublish();
    }


    @PostMapping
    public R<Course> addCourse(@RequestBody Course course) {
        return courseService.addCourse(course);
    }

    /**
     * 获取课程筛选类型
     * @return {@link R }<{@link HashMap }<{@link String }, {@link DictEnum[] }>>
     */
    @Operation (summary = "获取课程筛选类型", description = "")
    @GetMapping ("types")
    public R<HashMap<String, DictEnum[]>> types() {
        final HashMap<String, DictEnum[]> map = new HashMap<>();
        map.put("category", CourseCategory.values());
        map.put("type", CourseType.values());
        map.put("subject", CourseSubject.values());
        map.put("schoolLevel", CourseSchoolLevel.values());
        map.put("sort", CourseSort.values());
        return R.success(map);
    }

    /**
     * 首页 搜索
     * @param search   搜索
     * @param category 类别
     * @param type     类型
     * @param subject  主题
     * @param school   学校
     * @return {@link R }<{@link List }<{@link Map }<{@link String }, {@link String }>>>
     */
    @Operation(summary = "首页 搜索", description = "首页 搜索")
 
    @Parameters({
            @Parameter(name = "search", description = "搜索", in = ParameterIn.QUERY),
            @Parameter(name = "category", description = "类别", in = ParameterIn.QUERY),
            @Parameter(name = "type", description = "类型", in = ParameterIn.QUERY),
            @Parameter(name = "subject", description = "主题", in = ParameterIn.QUERY),
            @Parameter(name = "school", description = "学校", in = ParameterIn.QUERY),
            @Parameter(name = "sort", description = "", in = ParameterIn.QUERY),
            @Parameter(name = "page", description = "", in = ParameterIn.QUERY),
            @Parameter(name = "size", description = "", in = ParameterIn.QUERY)
    })
    @GetMapping("search")
    public R<List<Map<String, String>>> homeSearch(
            @RequestParam(value = "search", required = false, defaultValue = "") String search,
            @RequestParam(value = "category", required = false, defaultValue = "") String category,
            @RequestParam(value = "type", required = false, defaultValue = "") String type,
            @RequestParam(value = "subject", required = false, defaultValue = "") String subject,
            @RequestParam(value = "school", required = false, defaultValue = "") String school,
            @RequestParam(value = "sort", required = false, defaultValue = "") String sort,
            @RequestParam(value = "page", required = false, defaultValue = "") Integer page,
            @RequestParam(value = "size", required = false, defaultValue = "") Integer size
    ) {
        return courseService.homeSearch(search, category, type, subject, school, sort, page, size);
    }

    /**
     * 获取首页数据
     * @param obj
     * @param objVal
     * @param size
     * @return
     */
    @Operation(summary = "首页", description = "首页")
    @GetMapping("display")
    public R<List<Map<String, String>>> display(
            @RequestParam(value = "obj", required = false, defaultValue = "") String obj,
            @RequestParam(value = "objVal", required = false, defaultValue = "") String objVal,
            @RequestParam(value = "size", required = false, defaultValue = "") Integer size
    ) {
         return courseService.display(obj, objVal, size);
    }

    /**
     * 列出附件
     *
     * @param courseId 课程 ID
     * @return
     */
    @Parameter(name = "courseId", description = "课程 ID", in = ParameterIn.PATH, required = true)
    @Operation(summary = "列出附件", description = "列出附件")
    @GetMapping("attachments/{courseId}")
    public R<List<CourseAttachment>> listAttachments(@PathVariable("courseId") Integer courseId) {
        return attachmentService.listCourseAttachment(courseId);
    }

    /**
     * 直接上传附件
     *
     * @param courseId     课程 ID
     * @param courseItemId 关联课程项目ID
     * @param category     分类
     * @param file         附件内容
     * @return {@link R }<{@link CourseAttachment }>
     */
    @Parameters({
            @Parameter(name = "courseId", description = "课程 ID", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "courseItemId", description = "关联课程项目ID", in = ParameterIn.QUERY),
            @Parameter(name = "category", description = "分类", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "multipartFile", description = "附件内容", in = ParameterIn.QUERY, required = true, schema = @Schema(type = "string", format = "binary"))
    })
    @Operation(summary = "直接上传附件", description = "直接上传附件")
    @Parameter(style = ParameterStyle.FORM)
    @PostMapping("attachment/upload")
    public R<CourseAttachment> uploadAttachment(@RequestParam Integer courseId, @RequestParam(required = false) Integer courseItemId, @RequestParam SysFileCategory category, @RequestParam MultipartFile file) {
        return attachmentService.addAttachment(courseId, courseItemId, category, file);
    }

    /**
     * 后台课程列表条件查询
     * @param queryDTO
     * @return
     */
    @Operation(summary = "后台课程列表条件查询", description = "后台课程列表条件查询")
    @PostMapping("selectAll")
    public R<List<Course>> selectAll(CourseTableQueryDTO queryDTO){
        return  attachmentService.selectAll(queryDTO);
    }

    /**
     * 删除数据
     * @param ids
     * @return
     */
    @Operation(summary = "删除数据", description = "删除数据")
    @PostMapping("delete")
    public R<Boolean> delete(@RequestBody int[] ids) {
        return attachmentService.delete(ids);
    }

    /**
     * 推荐课程
     * @param user_id 用户 ID
     * @return {@link R }<{@link Course }>
     */
    @Parameter(name = "user_id", description = "用户 ID", in =  ParameterIn.PATH, required = true)
    @Operation(summary = "推荐课程",description = "")
    @GetMapping("recommend")
    public R<List<Map<String, String>>> getRecommendCourseByItems(@RequestParam(value = "user_id", required = true, defaultValue = "")Long user_id){
        return courseService.getRecommendCourseByItems(user_id);
    }

}
