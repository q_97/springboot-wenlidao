package com.wenlidao.web;

import cn.dev33.satoken.annotation.SaCheckRole;
import com.wenlidao.common.api.R;
import com.wenlidao.common.dto.UpdatePasswordDTO;
import com.wenlidao.common.dto.UserFormDTO;
import com.wenlidao.entity.User;
import com.wenlidao.service.user.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

@Tag (name = "用户API接口")
@RestController
@RequestMapping ("/api/user/")
public class UserController {
    @Resource
    UserService userService;

    @Operation (summary = "新增用户", description = "")
    @SaCheckRole ("Admin")
    @PostMapping("admin/add")
    public R<User> addUser(@RequestBody User user) {
        return R.success(userService.createUser(user)).msg("保存成功");
    }

    @Parameter (name = "username", description = "", in = ParameterIn.PATH, required = true)
    @Operation (summary = "查询单个用户", description = "")
    @SaCheckRole ("Admin")
    @GetMapping ("username/{username}")
    R<User> getUserByUsername(@PathVariable ("username") String username) {
        return R.success(userService.lambdaQuery().eq(User::getUsername, username).one());
    }

    @PostMapping("selectPage")
            R<List<User>> getUserWithPage( @RequestBody UserFormDTO params){
        return userService.getAllUser(params);
    }

    @Parameter (name = "id", description = "", in = ParameterIn.PATH, required = true)
    @Operation (summary = "查询单个用户", description = "")
    @SaCheckRole ("Admin")
    @GetMapping ("{id}")
    R<User> getUserById(@PathVariable ("id") Serializable id) {
        return R.success(userService.getById(id));
    }


/*    @Parameter (name = "id", description = "", in = ParameterIn.PATH, required = true)
    @Operation (summary = "用户列表查询", description = "")
//    @SaCheckPermission (value = "sys.user.list", orRole = Role.SUPER_ADMIN)
    @GetMapping ("/list")
    R<List<User>> list(UserTableQuery tableQuery) {
        return userService.getUserList(tableQuery);
    }*/

    @Operation (summary = "修改用户", description = "")
//    @SaCheckRole ("Admin")
    @PutMapping("admin/update")
    public R<User> updateUser(User user) {
        return R.success(userService.updateUser(user));
    }

    //    @Parameter (name = "id", description = "", in = ParameterIn.PATH, required = true)
    @Operation (summary = "删除用户", description = "")
    @SaCheckRole ("Admin")
    @DeleteMapping ("/del")
    public R<Boolean> delete(@RequestBody int[] ids) {
        return userService.delByIds(ids);
    }

    @PostMapping("updatePassword")
    public R<Void> updatePassword(UpdatePasswordDTO updatePasswordDTO){
        return userService.updatePassword(updatePasswordDTO);
    }
    /*
     * =========================
     *TODO:帐号相关的操作
     * */
    @Operation (summary = "绑定账户")
    @Parameters ( {
            @Parameter (name = "uid", description = "用户ID"),
            @Parameter (name = "type", description = "帐号类型"),
            @Parameter (name = "identifier", description = "帐号标识(帐号)")

    })
    @PostMapping ("account")
    public R<Boolean> bindAccount(
            @NotNull (message = "UID不能为空")
            Integer uid,
            @NotBlank (message = "帐号类型(type)不能为空")
            String type,
            @NotBlank (message = "帐号(identifier)不能为空")
            String identifier
    ) {
        userService.updateAccount(uid,type,identifier);
        return R.success(true).msg("绑定成功");
    }

    @Operation (summary = "解绑账户", description = "")
    @Parameters ( {
            @Parameter (name = "uid", description = "用户ID"),
            @Parameter (name = "type", description = "帐号类型")})
    @DeleteMapping ("account")
    public R<Boolean> deleteAccount(
            @NotNull (message = "UID不能为空")
            Integer uid,
            @NotBlank (message = "帐号类型(type)不能为空")
            String type) {
        userService.deleteAccount(uid, type);
        return R.success(true);
    }

}
