package com.wenlidao.web;

import com.wenlidao.common.api.R;
import com.wenlidao.common.util.ai.ChatAI;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@Tag (name = "chat")
@RestController
@RequestMapping ("/api/chat")
public class ChatController {
    ChatAI chatAI;

    public ChatController() {
        chatAI = new ChatAI("");
    }

    /**
     * 与 AI 聊天
     * @param prompt 提示
     * @return {@link R }<{@link String }>
     */
    @Operation (summary = "与 AI 聊天", description = "与 AI 聊天")
    @PostMapping ("/stream")
    public R<String> chatWithAI(String prompt) throws IOException {
        final String s = chatAI.get("userId", prompt);
        return R.success(s);
    }
}
