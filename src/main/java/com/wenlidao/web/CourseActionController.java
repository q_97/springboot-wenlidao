package com.wenlidao.web;

import com.wenlidao.common.api.R;
import com.wenlidao.common.vo.course.CourseIssue;
import com.wenlidao.common.vo.course.CourseReply;
import com.wenlidao.entity.course.Course;
import com.wenlidao.entity.course.CourseAction;
import com.wenlidao.entity.enumerate.ActionType;
import com.wenlidao.service.course.action.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 课程互动API
 *
 * @author INYXIN
 * @version 1.0.0
 * 4月 5, 2024   20:54
 */
@Tag(name = "课程互动API 4月 5, 2024   20:54", description = "课程互动API 4月 5, 2024   20:54")
@RestController
@RequestMapping("/course/action")
public class CourseActionController {
    /**
     * 操作服务
     */
    @Resource
    ActionService actionService;
    @Resource
    SelectedCourseService selectedCourseService;
    @Resource
    AnnouncementService announcementService;
    @Resource
    IssueService issueService;
    @Resource
    ReplyService replyService;

    /**
     * 记录浏览操作
     *
     * @param courseId 课程 ID
     * @return {@link R }<{@link Void }>
     */
    @Parameter(name = "courseId", description = "课程 ID", in = ParameterIn.PATH, required = true)
    @Operation(summary = "记录浏览操作", description = "记录浏览操作")
    @PutMapping("view/{courseId}")
    public R<Void> recordView(@PathVariable("courseId") Integer courseId) {
        return actionService.recordView(courseId);
    }

    /**
     * 记录选课操作
     *
     * @param courseId 课程 ID
     * @return {@link R }<{@link Void }>
     */
    @Parameter(name = "courseId", description = "课程 ID", in = ParameterIn.PATH, required = true)
    @Operation(summary = "记录选课操作", description = "记录选课操作")
    @PutMapping("selected/{courseId}")
    public R<Void> selectedCourse(@PathVariable("courseId") Integer courseId) {
        return selectedCourseService.selectedCourse(courseId);
    }

    /**
     * 取消选课操作
     *
     * @param courseId 课程 ID
     * @return {@link R }<{@link Void }>
     */
    @Parameter(name = "courseId", description = "课程 ID", in = ParameterIn.PATH, required = true)
    @Operation(summary = "取消选课操作", description = "取消选课操作")
    @PutMapping("cancelSelected/{courseId}")
    public R<Void> cancelSelectedCourse(@PathVariable("courseId") Integer courseId) {
        return selectedCourseService.cancelSelected(courseId);
    }

    /**
     * 列出当前所选课程
     *
     * @return {@link R }<{@link List }<{@link Course }>>
     */
    @Operation(summary = "获取当前用户所选课程",description = "获取当前用户所选课程")
    @GetMapping("selected")
    public R<List<Course>> listSelectedCourse() {
        return selectedCourseService.listSelectedCourse();
    }

    /**
     * 获取课程统计数据
     *
     * @param courseId 课程 ID
     * @return {@link R }<{@link Course }>
     */
    @Parameter(name = "courseId", description = "课程 ID", in = ParameterIn.PATH, required = true)
    @Operation(summary = "获取课程统计数据", description = "")
    @GetMapping("/statistics/{courseId}")
    public R<Map<ActionType, Long>> getCourseStatistics(@PathVariable("courseId") Integer courseId) {
        return actionService.getCourseActionMetaData(courseId);
    }

    /**
     * 获取课程公告
     *
     * @param courseId 课程 ID
     * @return {@link R }<{@link List }<{@link CourseAction }>>
     */
    @Parameter(name = "courseId", description = "课程 ID", in = ParameterIn.PATH, required = true)
    @Operation(summary = "获取课程公告", description = "")
    @GetMapping("/notice/{courseId}")
    public R<List<CourseAction>> getAnnouncements(@PathVariable("courseId") Integer courseId) {
        return announcementService.getAnnouncements(courseId);
    }

    /**
     * 添加或保存公告
     *
     * @param id       公告ID
     * @param courseId 课程 ID
     * @param content  内容
     * @return {@link R }<{@link CourseAction }>
     */
    @Parameters({@Parameter(name = "id", description = "公告ID", in = ParameterIn.QUERY), @Parameter(name = "courseId", description = "课程 ID", in = ParameterIn.QUERY, required = true), @Parameter(name = "content", description = "内容", in = ParameterIn.QUERY, required = true)})
    @Operation(summary = "添加或保存公告", description = "添加或保存公告")
    @PostMapping("/notice")
    public R<CourseAction> saveNotice(@RequestParam(required = false) Integer id, @RequestParam Integer courseId, @RequestParam String content) {
        if (id == null) return actionService.addAnnouncement(courseId, content);
        else return announcementService.updateAnnouncement(id, courseId, content);
    }

    /**
     * 删除课程公告
     *
     * @param courseId 课程 ID
     * @return {@link R }<{@link List }<{@link CourseAction }>>
     */
    @Parameter(name = "courseId", description = "课程 ID", in = ParameterIn.PATH, required = true)
    @Operation(summary = "删除课程公告", description = "")
    @DeleteMapping("/notice/{courseId}")
    public R<Boolean> deleteAnnouncement(@PathVariable("courseId") Integer courseId) {
        return announcementService.deleteAnnouncement(courseId);
    }

    /**
     * 获取课程Issue问题列表
     *
     * @param courseId 课程 ID
     * @return {@link R }<{@link List }<{@link CourseIssue }>>
     */
    @Parameter(name = "courseId", description = "课程 ID", in = ParameterIn.PATH, required = true)
    @Operation(summary = "获取课程Issue问题列表", description = "获取课程Issue问题列表")
    @GetMapping("issues/{courseId}")
    public R<List<CourseIssue>> getIssues(@PathVariable("courseId") Integer courseId) {
        return issueService.getIssues(courseId);
    }

    /**
     * 添加问题
     *
     * @param courseId 课程 ID
     * @param content  内容
     * @return {@link R }<{@link CourseIssue }>
     */
    @Parameters({@Parameter(name = "courseId", description = "课程 ID", in = ParameterIn.QUERY, required = true), @Parameter(name = "content", description = "内容", in = ParameterIn.QUERY, required = true)})
    @Operation(summary = "添加问题", description = "添加问题")
    @PostMapping("issue")
    public R<CourseIssue> addIssue(@RequestParam Integer courseId, @RequestParam String content) {
        return issueService.addIssue(courseId, content);
    }


    /**
     * 获取回复
     *
     * @param issueId 问题 ID
     * @return
     */
    @Parameter(name = "issueId", description = "问题 ID", in = ParameterIn.PATH, required = true)
    @Operation(summary = "获取回复", description = "获取回复")
    @GetMapping("replies/{issueId}")
    public R<List<CourseReply>> getReplies(@PathVariable("issueId") Integer issueId) {
        return replyService.getReplies(issueId);
    }

    /**
     * 添加issue回复
     *
     * @param issueId 问题 ID
     * @param content 内容
     * @return {@link R }<{@link CourseReply }>
     */
    @Parameters({@Parameter(name = "issueId", description = "问题 ID", in = ParameterIn.QUERY, required = true), @Parameter(name = "content", description = "内容", in = ParameterIn.QUERY, required = true)})
    @Operation(summary = "添加issue回复", description = "添加issue回复")
    @PostMapping("reply")
    public R<CourseReply> addReply(@RequestParam Integer courseId, @RequestParam Integer issueId, @RequestParam String content) {
        return replyService.addReply(courseId, issueId, content);
    }


    /**
     * 更新进度
     * 记录一下看到第几集了
     *
     * @param courseId 课程 ID
     * @param itemId   课程项 ID
     * @return {@link R }<{@link Void }>
     */
    @PutMapping("process/{courseId}/{itemId}")
    public R<Void> updateProgress(@PathVariable Integer courseId, @PathVariable Integer itemId) {
        return actionService.updateProgress(courseId, itemId);
    }

    /**
     * 查询
     * 记录一下看到第几集了
     *
     * @param courseId 课程 ID
     * @return {@link R }<{@link Integer }>
     */
    @GetMapping("process/{courseId}")
    public R<Integer> getProcess(@PathVariable Integer courseId) {
        final CourseAction progress = actionService.getProgress(courseId);
        Integer p = progress == null ? 0 : progress.getTargetId();
        return R.success(p);
    }

}
