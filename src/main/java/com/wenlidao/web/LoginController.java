package com.wenlidao.web;


import com.wenlidao.common.api.R;
import com.wenlidao.common.dto.EmailLoginForm;
import com.wenlidao.common.dto.UserRegisterForm;
import com.wenlidao.common.dto.UsernameLoginForm;
import com.wenlidao.common.vo.ImageCaptcha;
import com.wenlidao.common.vo.LoginResult;
import com.wenlidao.entity.User;
import com.wenlidao.service.CaptchaService;
import com.wenlidao.service.user.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import me.zhyd.oauth.model.AuthCallback;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Slf4j
@Tag(name = "登录API")
@RestController
@RequestMapping("/api/auth")
public class LoginController {
    @Resource
    UserService userService;
    @Resource
    CaptchaService captchaService;

    @Operation(summary = "获取登录信息", description = "")
    @GetMapping("loginInfo")
    public R<LoginResult> loginInfo(@RequestHeader("Authorization") String token) {
        System.out.println(token);
        return userService.getLoginInfo();
    }


    @Operation(summary = "退出登录", description = "")
    @GetMapping("logout")
    public R logout() {
        return userService.logout();
    }


    @Operation(summary = "通过用户名登录", description = "")
    @PostMapping("login/username")
    public R<LoginResult> login(@Validated UsernameLoginForm loginForm) {
        return userService.login(loginForm);
    }

    @Operation(summary = "通过邮箱登录", description = "")
    @PostMapping("login/email")
    public R<LoginResult> login(@Validated EmailLoginForm loginForm) {
        return userService.login(loginForm);
    }


    @Operation(summary = "用户注册")
    @PostMapping(value = "register")
    public R<User> register(@Validated UserRegisterForm registerForm) {
        final User register = userService.register(registerForm);
        return R.success(register).msg("注册成功");
    }

    /*
     * oauth2
     * */

    /**
     * 重定向到认证页面
     *
     * @param source oauth供应商 如 gitee ,github
     * @throws IOException ioexception
     */
    @Parameter(name = "source", description = "oauth供应商 如 gitee ,github", in = ParameterIn.PATH, required = true, examples = {@ExampleObject("gitee"), @ExampleObject("github")})
    @Operation(summary = "重定向到OAuth2认证页面", description = "重定向到认证页面")
    @GetMapping("oauth2/authorize/{source}")
    public void renderAuth(@PathVariable("source") String source, @Parameter(hidden = true) HttpServletResponse response) throws IOException {
        userService.oauth2Authorize(response, source);
    }

    @Parameter(name = "source", description = "认证源", in = ParameterIn.PATH, required = true)
    @Operation(summary = "OAuth2授权成功后回调登录", description = "", hidden = true)
    @RequestMapping(value = "oauth2/code/{source}", method = {RequestMethod.GET, RequestMethod.POST})
    public void login(@PathVariable("source") String source, @Parameter(description = "认证回调参数") AuthCallback callback, HttpServletResponse response) throws IOException {
        userService.oauth2Callback(source, callback, response);
    }

    /**
     * 获取图片验证码
     *
     * @return {@link R }<{@link ImageCaptcha }>
     * @throws IOException ioexception
     */
    @Operation(summary = "获取图片验证码", description = "获取图片验证码")
    @GetMapping("captcha/image")
    public R<ImageCaptcha> captcha() throws IOException {
        return captchaService.createImageCaptcha();
    }

    /**
     * 通过电子邮件发送验证码
     *
     * @param email 电子邮件
     */
    @Parameter(name = "email", description = "电子邮件", in = ParameterIn.QUERY, required = true)
    @Operation(summary = "通过电子邮件发送验证码", description = "通过电子邮件发送验证码")
    @GetMapping("captcha/email")
    public R<Object> emailCaptcha(@RequestParam("email") String email) {
        log.debug("发送邮件验证码到:{}", email);
        return captchaService.createEmailCaptcha(email);
    }
}
