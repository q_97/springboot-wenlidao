package com.wenlidao.web;

import com.wenlidao.common.api.R;
import com.wenlidao.common.exception.AppException;
import com.wenlidao.entity.SysFile;
import com.wenlidao.entity.course.Course;
import com.wenlidao.entity.course.CourseItem;
import com.wenlidao.entity.enumerate.DictEnum;
import com.wenlidao.entity.enumerate.SysFileCategory;
import com.wenlidao.service.ISysFileService;
import com.wenlidao.service.course.CourseItemService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 课程项API
 * @author INYXIN
 * @version 1.0.0
 */
@Tag (name = "课程项API", description = "课程项API")
@RestController
@RequestMapping ("/course/item")
public class CourseItemController {
    @Resource
    CourseItemService courseItemService;
    @Resource
    ISysFileService fileService;

    /**
     * 列出课程的所有小节
     * @param courseId 课程 ID
     * @return {@link R }<{@link List }<{@link DictEnum }>>
     */
    @Operation (summary = "列出课程的所有小节", description = "列出课程的所有小节")
    @GetMapping ("list")
    public R<List<DictEnum>> selectionCourseItem(Integer courseId) {
        return courseItemService.selectionCourseItem(courseId);
    }

    /**
     * 获取课程和项目
     * @param courseId 课程 ID
     * @return {@link R }<{@link Course }>
     */
    @Parameter (name = "courseId", description = "课程 ID", in = ParameterIn.PATH, required = true)
    @Operation (summary = "获取课程和项目", description = "获取课程和项目")
    @GetMapping ("items/{courseId}")
    public R<Course> getCourseAndItems(@PathVariable Integer courseId) {
        return courseItemService.getCourseAndItems(courseId);
    }

    /**
     * 上传课程项目(课程项)
     * @return {@link R }<{@link CourseItem }>
     */
    @Operation (summary = "上传课程项目(课程项)", description = "上传课程项目(课程项)")
    @PostMapping
    public R<CourseItem> uploadCourseItem(
            @RequestParam ("name")
            @NotBlank (message = "视频名必填")
            String name,
            @RequestParam ("courseId")
            @NotNull (message = "课程ID必填")
            Integer courseId,
            @RequestParam ("file")
            MultipartFile file
    ) {
        SysFile upload = fileService.upload(file, SysFileCategory.Video);
        String accessSrc = fileService.getAccessSrc(upload);
        CourseItem courseItem = new CourseItem()
                .setCourseId(courseId)
                .setVideo(accessSrc)
                .setName(name)
                .setDuration("10分钟");
        final boolean b = this.courseItemService.saveOrUpdate(courseItem);
        if (b) return R.success(courseItem).msg("课程上传成功");
        throw new AppException("上传课程项失败");
    }

    /**
     * 删除课程项目
     * @param ids id集合
     * @return {@link R }<{@link Void }>
     */
    @DeleteMapping
    public R<Void> deleteCourseItem(@RequestBody List<Integer> ids) {
        final boolean b = courseItemService.removeBatchByIds(ids);
        return b ? R.success() : R.fail().msg("删除失败");
    }
}
