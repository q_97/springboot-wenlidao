package com.wenlidao.common.vo;

import lombok.Data;

@Data
public class CourseBackVO {
    private String id;
    private String name;
    private String teacherName;
    private String subject;
}
