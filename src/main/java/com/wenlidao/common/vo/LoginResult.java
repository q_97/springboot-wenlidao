package com.wenlidao.common.vo;


import cn.dev33.satoken.stp.SaTokenInfo;
import com.wenlidao.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginResult {
    private SaTokenInfo token;
    private User user;
}
