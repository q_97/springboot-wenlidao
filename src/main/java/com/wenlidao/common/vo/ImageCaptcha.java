package com.wenlidao.common.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Schema (description = "图片验证码")
@NoArgsConstructor
@AllArgsConstructor
public class ImageCaptcha {
    @Schema (description = "验证码唯一标识")
    private String uuid;
    @Schema (description = "base64图片验证码")
    private String base64;
    @Schema (description = "过期时间")
    private String expire;
}
