package com.wenlidao.common.vo.course;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;
@Data
@Accessors(chain = true)
@Schema(description = "课程回答")
public class CourseReply {
    @Schema (description = "id")
    Integer id;
    @Schema (description = "内容")
    String content;
    @Schema(description = "回答时间")
    Date time;
    @Schema (description = "回答者ID")
    Integer userId;
    @Schema (description = "用户名")
    String username;
    @Schema (description = "用户昵称")
    String nickname;
    @Schema (description = "用户头像")
    String avatar;
}
