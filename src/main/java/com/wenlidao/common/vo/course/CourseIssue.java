package com.wenlidao.common.vo.course;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 课程提问
 * @author INYXIN
 * @version 1.0.0
 * 3月 29, 2024   17:03
 */
@Data
@Accessors(chain = true)
@Schema (description = "课程提问")
public class CourseIssue {
    @Schema (description = "issueForm ID")
    Integer id;
    @Schema (description = "内容")
    String content;
    @Schema(description = "提问时间")
    Date time;
    @Schema (description = "回答数量")
    Integer count;
    @Schema (description = "提问用户ID")
    Integer userId;
    @Schema (description = "提问用户名")
    String username;
    @Schema (description = "提问用户昵称")
    String nickname;
    @Schema (description = "提问用户头像")
    String avatar;
}
