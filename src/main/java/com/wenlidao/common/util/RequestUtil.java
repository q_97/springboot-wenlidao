package com.wenlidao.common.util;


import okhttp3.OkHttpClient;
import okhttp3.Request;

/**
 * <p>Class: {@link RequestUtil} Api工具类</p>
 * <p>Author : Alpha</p>
 * <p>Date : 8月 30, 2023   13:02</p>
 * <p>Description: </p>
 * <p></p>
 */
public class RequestUtil {
    public static OkHttpClient client = new OkHttpClient();


    /**
     * <p>创建请求构建器</p>
     * <p>添加了一些常用请求头.反爬虫</p>
     * @return {@link Request.Builder }
     * @see Request.Builder
     */
    public static Request.Builder createRequestBuilder() {
        Request.Builder request = new Request.Builder();
        request.header("Accept", "*/*");
        request.header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36 Edg/116.0.1938.54");
        request.header("Sec-Fetch-Site", "same-origin");
        request.header("Sec-Fetch-Mode", "no-cors");
        request.header("Sec-Ch-Ua-Platform", "\"Windows\"");
        request.header("Sec-Ch-Ua-Mobile", "?0");
        request.header("Sec-Ch-Ua", "\"Chromium\";v=\"116\", \"Not)A;Brand\";v=\"24\", \"Microsoft Edge\";v=\"116\"");
        request.header("Pragma", "no-cache");
        request.header("Cache-Control", "no-cache");
        request.header("Connection", "keep-alive");
        request.header("Accept-Language", "zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6,pt;q=0.5,pl;q=0.4");
        return request;
    }

}
