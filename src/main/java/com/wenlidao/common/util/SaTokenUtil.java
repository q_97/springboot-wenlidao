package com.wenlidao.common.util;

import cn.dev33.satoken.stp.StpUtil;

public class SaTokenUtil {
    //测试环境
    public static final boolean testEnvironment = false;

    /**
     * 获取登录 ID,如果是测试环境,则永远返回1
     * @return int
     */
    public static int getLoginId() {
        return testEnvironment ? 1 : StpUtil.getLoginIdAsInt();
    }
}
