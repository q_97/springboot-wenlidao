package com.wenlidao.common.util.lucene;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * INDEX 索引注解
 * @author INYXIN
 * @version 1.0.0
 * 4月 3, 2024   21:17
 */
@Retention (RetentionPolicy.RUNTIME)
public @interface IndexField {
    /**
     * 是否索引 (是否作为查询条件)
     * @return boolean
     */
    boolean indexed() default true;
    /**
     * 是否分词
     * @return boolean
     */
    boolean tokenized() default true;

    /**
     * 是否存储
     * * <pre>商品名称、订单号，凡是将来要从Document中获取的Field都要存储。</pre
     * @return boolean
     */
    boolean stored() default true;
}
