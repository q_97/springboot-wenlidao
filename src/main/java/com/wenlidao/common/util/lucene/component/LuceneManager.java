package com.wenlidao.common.util.lucene.component;

import com.wenlidao.common.util.lucene.LuceneUtil;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import java.io.IOException;
import java.util.Arrays;

/**
 * 索引管理器
 * <p>主要是维护索引,如增删改等等</p>
 * @author INYXIN
 * @version 1.0.0
 * 4月 3, 2024   23:16
 */
@Slf4j

public class LuceneManager {
    @Getter
    private IndexWriter writer;

    public LuceneManager(FSDirectory directory) throws IOException {
        final IndexWriterConfig writerConfig = new IndexWriterConfig(Version.LATEST, LuceneUtil.getAnalyzer());
        writerConfig.setOpenMode(OpenMode.CREATE_OR_APPEND);
        writer = new IndexWriter(directory, writerConfig);
    }

    /**
     * 添加文档(可批量添加)
     * @param docs 文档
     */
    public void addDocuments(Document... docs) throws IOException {
        writer.addDocuments(Arrays.stream(docs).toList());
        writer.commit();
    }

    /**
     * 删除文档
     * @param terms 条款
     */
    public void deleteDocuments(Term... terms) throws IOException {
        writer.deleteDocuments(terms);
        writer.commit();
    }

    /**
     * 更新文档
     * @param term     术语
     * @param document 公文
     */
    public void updateDocument(Term term, Document document) throws IOException {
        writer.updateDocument(term, document);
        writer.commit();
    }

    /**
     * 添加文档
     * @param beans 自定义实体对象
     */
    public void addDocuments(Object... beans) throws IOException {
        final DocConverter converter = LuceneUtil.getConverter();
        final Document[] documents = Arrays.stream(beans).map(converter::convert).toArray(Document[]::new);
        addDocuments(documents);
    }

    public void commit() {
        try {
            writer.commit();
        } catch(IOException e) {
            try {
                writer.rollback();
            } catch(IOException ignored) {
                log.error("回滚异常");
            }
        }
    }

    public void deleteAllDocuments() throws IOException {
        writer.deleteAll();
        writer.commit();
    }
}
