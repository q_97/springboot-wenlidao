package com.wenlidao.common.util.lucene.component;

import com.wenlidao.common.util.lucene.IndexField;
import com.wenlidao.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexableField;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 文档转换器
 * @author INYXIN
 * @version 1.0.0
 * 4月 3, 2024   21:57
 */
@Slf4j
public class DocConverter {
    /**
     * 自定义对象转文档
     * @param object 对象
     * @return {@link Document }
     */
    public <T> Document convert(T object) {
        final Document document = new Document();
        final Class<?> _class = object.getClass();
        //此值仅用于索引查询,分类,不存储,不分词
        document.add(new StringField("__class__", _class.getName(), Store.NO));
        for (java.lang.reflect.Field declaredField : _class.getDeclaredFields()) {
            declaredField.setAccessible(true);
            final Object value;
            try {
                value = declaredField.get(object);
            } catch(IllegalAccessException e) {
                throw new RuntimeException(e);
            }
            //只处理值值不为null 且带有注解的字段
            if (declaredField.isAnnotationPresent(IndexField.class) && value != null) {
                    document.add(createField(value.toString(), declaredField));
            }
        }
        return document;
    }

    /**
     * 创建字段
     * @param value         价值
     * @param declaredField 声明字段
     * @return {@link IndexableField }
     */
    private IndexableField createField(String value, java.lang.reflect.Field declaredField) {
        final IndexField annotation = declaredField.getAnnotation(IndexField.class);
        final String name = declaredField.getName();
        final Store stored = annotation.stored() ? Store.YES : Store.NO;
        if (annotation.indexed()) {
            if (annotation.tokenized()) {
                return new TextField(name, value, stored);
            } else {
                return new StringField(name, value, stored);
            }
        } else {
            return new StoredField(name, value);
        }
    }

    /**
     * 转换成HashMap
     * @param document 公文
     * @return {@link HashMap }<{@link String }, {@link String }>
     */
    public Map<String, String> convert(Document document) {
        return document.getFields().stream()
                       .collect(Collectors
                               .toMap(IndexableField::name, IndexableField::stringValue));

    }
}
