package com.wenlidao.common.util.lucene.component;

import com.wenlidao.entity.User;
import org.apache.lucene.analysis.Analyzer;
import com.wenlidao.common.api.R;
import com.wenlidao.common.util.lucene.LuceneUtil;
import com.wenlidao.entity.course.Course;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.search.highlight.*;
import org.apache.lucene.search.highlight.Formatter;
import org.apache.lucene.store.FSDirectory;
import org.wltea.analyzer.lucene.IKAnalyzer;

import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;


/**
 * 索引查询器
 * <p>主要功能是对文档 索引进行查询</p>
 *
 * @author INYXIN
 * @version 1.0.0
 * 4月 3, 2024   23:15
 */
public class LuceneSearcher {
    private final FSDirectory directory;

    private DirectoryReader reader;

    public LuceneSearcher(FSDirectory directory) throws IOException {
        this.directory = directory;
    }


    /**
     * 获取最新的Reader
     *
     * @return {@link DirectoryReader }
     */
    public DirectoryReader getReader() {
        try {
            if (reader == null) {
                this.reader = DirectoryReader.open(directory);
                return reader;
            }
            final DirectoryReader newReader = DirectoryReader.openIfChanged(reader);
            if (newReader != null) this.reader = newReader;
            return reader;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 简单搜索
     *
     * @param field 字段
     * @param value 值
     * @param count 数量
     * @return {@link List }<{@link Document }>
     */
    public List<Document> search(String field, String value, int count) throws IOException, ParseException {
        final Query query = new QueryParser(field, LuceneUtil.getAnalyzer()).parse(value);
        final IndexSearcher searcher = new IndexSearcher(getReader());
        final TopDocs docs = searcher.search(query, count);
        return Arrays.stream(docs.scoreDocs).map(scoreDoc -> {
            try {
                return searcher.doc(scoreDoc.doc);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }).toList();
    }

    public R<List<Map<String, String>>> search(Query query, int page, int size, String sort) throws IOException {
        if (page < 1 || size < 1) throw new IllegalArgumentException("page和size必须大于0");
        final IndexSearcher searcher = new IndexSearcher(getReader());
        final TopDocs preSearch = searcher.search(query, 1);
        final int totalHits = preSearch.totalHits;//预搜索 发现命中总数量
        final int start = Math.max(0, (page - 1) * size); //确定开始位置
        final int end = Math.min(start + size, totalHits); //确定结束位置
//        final int end = totalHits;
        List<Map<String, String>> list = new ArrayList<>();
        if (totalHits > 0) {//只有预查询命中数>0才进行分页
            // 格式化器
            Formatter formatter = new SimpleHTMLFormatter("<span style='color:blue'>", "</span>");
            QueryScorer scorer = new QueryScorer(query);
            // 准备高亮工具
            Highlighter highlighter = new Highlighter(formatter, scorer);

            //查询 [0,end] 的记录
            final TopDocs topDocs = searcher.search(query, end);
            //取出分页结果 [start, end]
            for (int i = start; i < end; i++) {
                final float score = topDocs.scoreDocs[i].score;
                final int docID = topDocs.scoreDocs[i].doc;
                final Document doc = searcher.doc(docID);
                final String name = doc.get("name");
                String hName;
                try {
                    hName = highlighter.getBestFragment(new IKAnalyzer(), "name", name);
                } catch (InvalidTokenOffsetsException e) {
                    throw new RuntimeException(e);
                }
                final Map<String, String> map = LuceneUtil.getConverter().convert(doc); //将doc转换成map
                map.put("hName", hName);
                map.put("_score", String.valueOf(score));
                map.put("_doc", String.valueOf(docID));
                list.add(map);
            }
            if (sort.equals("Latest")) {
                list.sort((a, b) -> {
                    Date dateA, dateB;
                    try {
                        dateA = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(a.get("datetime"));
                        dateB = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(b.get("datetime"));

                    } catch (java.text.ParseException e) {
                        throw new RuntimeException(e);
                    }
                    return (int) (dateB.getTime() - dateA.getTime());
                });
            }
        }
        return R.success(list).msg("查询成功").put("total", totalHits)//总数
                .put("page", page)//当前页
                .put("start", start)//开始位置
                .put("end", end)//结束位置
                .put("count", list.size())//本次查询数量
                .put("size", size)//单页数量
                ;
    }

    /**
     * 通过数据id精确查找数据
     *
     * @param Id 数据ID
     */
    public R<List<Map<String, String>>> PreciseQueries(List<Long> Id) throws IOException {
        List<Map<String, String>> result = new ArrayList<>();
        final IndexSearcher searcher = new IndexSearcher(getReader());

        for (Long externalId : Id) {
            Query query = new TermQuery(new Term("id", externalId.toString()));

            TopDocs topDocs = searcher.search(query, 1);
            ScoreDoc[] hits = topDocs.scoreDocs;
            if (hits.length > 0) {
                Document document = searcher.doc(hits[0].doc);
                Map<String, String> docData = new HashMap<>();
                for (IndexableField field : document) {
                    docData.put(field.name(), field.stringValue());
                }
                result.add(docData);
            }
        }
        return R.success(result);
    }
}
