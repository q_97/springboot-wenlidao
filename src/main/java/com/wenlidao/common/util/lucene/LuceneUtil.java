package com.wenlidao.common.util.lucene;

import com.wenlidao.common.util.lucene.component.DocConverter;
import com.wenlidao.common.util.lucene.component.LuceneManager;
import com.wenlidao.common.util.lucene.component.LuceneSearcher;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.store.FSDirectory;
import org.wltea.analyzer.lucene.IKAnalyzer;

import java.io.File;
import java.io.IOException;

/**
 * Lucene 搜索引擎工具类
 * @author INYXIN
 * @version 1.0.0
 * 4月 3, 2024   23:59
 */
@Slf4j
public class LuceneUtil {
    //TODO:修改lucene 的索引库目录, 默认是使用相对路径项目根路径下的lucene_index
    private static final File INDEX_PATH = new File("lucene_index");
    //索引目录
    @Getter
    private static final FSDirectory directory;
    /**
     * 索引查询器
     */
    @Getter
    private static final LuceneSearcher searcher;
    /**
     * 索引管理器
     */
    @Getter
    private static final LuceneManager manager;
    /**
     * 文档转换器
     */
    @Getter
    private static final DocConverter converter;
    /**
     * 分词器
     */
    @Getter
    static Analyzer analyzer;

    private LuceneUtil() {//私有化构造
    }

    static {
        if (!INDEX_PATH.exists() && !INDEX_PATH.isDirectory() && !INDEX_PATH.mkdirs()) {
            log.error("创建索引库目录失败: {}", INDEX_PATH);
        }
        try {
            directory = FSDirectory.open(INDEX_PATH);
            //初始化组件
            analyzer = new IKAnalyzer();
            converter = new DocConverter();
            manager = new LuceneManager(directory);
            searcher = new LuceneSearcher(directory);
            manager.commit();
        } catch(IOException e) {
            log.error("初始化LuceneUtil失败", e);
            throw new RuntimeException(e);
        }
    }
}
