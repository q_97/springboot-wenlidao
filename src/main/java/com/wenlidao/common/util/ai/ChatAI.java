package com.wenlidao.common.util.ai;

import com.alibaba.fastjson2.JSONObject;
import com.wenlidao.common.util.RequestUtil;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import java.io.IOException;

public class ChatAI {
    /*系统提示词 (合适的系统提示词 可以完成角色扮演之类的)*/
    private final String system;

    public ChatAI(String system) {
        this.system = system;
    }

    /**
     * AI聊天
     * @param session 一般使用同一个id 保持一个AI会话
     * @param prompt  问题
     * @return {@link String}
     */
    public String get(String session, String prompt) throws IOException {
        JSONObject data = new JSONObject();
        data.put("prompt", prompt);
        data.put("userId", "#/chat/" + session);
        data.put("network", true);
        data.put("system", this.system);
        data.put("withoutContext", false);
        data.put("stream", true);
        Request.Builder request = RequestUtil.createRequestBuilder();
        request.header("Referer", "https://chat18.aichatos.xyz/");
        request.header("Origin", "https://chat18.aichatos.xyz");
        request.header("Content-Type", "application/json");
        request.url("https://api.binjie.fun/api/generateStream");
        RequestBody requestBody = RequestBody.create(data.toJSONString(), MediaType.parse("application/json"));
        Request.Builder post = request.post(requestBody);
        Response response = RequestUtil.client.newCall(post.build()).execute();
        return response.body().string();
    }
}