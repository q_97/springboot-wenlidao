package com.wenlidao.common.util;

import com.wenlidao.common.exception.AppException;
import jakarta.annotation.Resource;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * 电子邮件工具类
 * @author INYXIN
 * @version 1.0.0
 * 1月 28, 2024   16:00
 */
@Component
@Slf4j
public class EmailUtil {
    @Value ("${spring.application.name}")
    private String APPLICATION_NAME;
    @Resource
    private JavaMailSenderImpl mailSender;
    /**
     * 电子邮件模板
     *
     * 原模板在resource下
     * <a href="https://www.jyshare.com/front-end/47/">菜鸟教程 HTML压缩</a>
     */
    public static final String EMAIL_TEMPLATE ="<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"UTF-8\"><title>${application}</title><style>*{font-family:Corbel,幼圆,serif}</style></head><body><div style=\"display: flex;align-items: center;justify-content: center;\"><div class=\"card\"style=\"width: 300px; height: auto; margin: 0 auto; background-color: #011522; border-radius: 8px; z-index: 1; \"><div class=\"tools\"style=\"display: flex; align-items: center; padding: 9px;\"><div class=\"circle\"style=\"  padding: 0 4px;\"><span class=\"red box\"style=\"            background-color: #ff605c; display: inline-block; align-items: center; width: 10px; height: 10px; padding: 1px; border-radius: 50%;\"></span></div><div class=\"circle\"style=\"  padding: 0 4px;\"><span class=\"yellow box\"style=\"        background-color: #ffbd44;display: inline-block; align-items: center; width: 10px; height: 10px; padding: 1px; border-radius: 50%;\"></span></div><div class=\"circle\"style=\"  padding: 0 4px;\"><span class=\"green box\"style=\"             background-color: #00ca4e;display: inline-block; align-items: center; width: 10px; height: 10px; padding: 1px; border-radius: 50%;\"></span></div></div><div class=\"card__content\"><div style=\"display: flex;align-items: center;flex-direction: column;padding:20px 30px\"><h3 style=\"color: #FFF4E0\">嘿!你在${application}中收到一条消息。</h3><p style=\"color:#A69984BA\">你收到来自${from}的消息</p><div style=\"padding:15px;display: flex;flex-direction: column;width: 100%\"><h3 style=\"color: #de4c4c\">${subject}</h3><p style=\"color:#FFF4E0\">${content}</p></div><p style=\"color: #A69984\">此消息由${application}自动发出,直接回复无效</p></div></div></div></div></body></html>";

    /**
     * 发送邮件
     * @param subject 主题 (标题)
     * @param content 邮件内容
     * @param emails  接收方电子邮件
     */
    public void sentEmail(String subject, String content, String... emails)  throws AppException{
        log.info("发送邮件 -> {} : {} : {}", Arrays.toString(emails), subject, content);
        MimeMessage message = mailSender.createMimeMessage();
        try {
            message.setHeader("Content-Type", "text/html; charset=UTF-8");
            MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
            helper.setFrom("inyxin@qq.com");
            helper.setTo(emails);
            helper.setSubject(subject);
            //设置邮件内容
            helper.setText(EMAIL_TEMPLATE.replace("${application}", APPLICATION_NAME)
                                         .replace("${from}", APPLICATION_NAME)
                                         .replace("${subject}", subject)
                                         .replace("${content}", content), true);
            mailSender.send(message);
        } catch(MessagingException e) {
            log.error("发送邮件失败" + e.getLocalizedMessage(), e);
            throw new AppException("邮件服务异常");
        }
    }
}
