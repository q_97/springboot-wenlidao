package com.wenlidao.common.util.sensitive.word;

import com.github.houbb.sensitive.word.api.IWordAllow;
import com.github.houbb.sensitive.word.api.IWordDeny;
import com.github.houbb.sensitive.word.bs.SensitiveWordBs;
import com.github.houbb.sensitive.word.support.deny.WordDenys;
import jakarta.annotation.Resource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringSensitiveWordConfig {
    @Resource
    IWordAllow allow;
    @Resource
    IWordDeny deny;

    @Bean
    public SensitiveWordBs getInstance() {
        return SensitiveWordBs.newInstance()
                              .wordAllow(allow)
                              .ignoreRepeat(true)
                              .wordDeny(WordDenys.chains(WordDenys.defaults(), deny))
                              .init();
    }
}
