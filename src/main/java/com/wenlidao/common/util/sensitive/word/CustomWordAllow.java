package com.wenlidao.common.util.sensitive.word;

import com.github.houbb.sensitive.word.api.IWordAllow;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

/**
 * 自定义允许词汇
 * @author INYXIN
 * @version 1.0.0
 * 4月 10, 2024   19:13
 */
@Component
public class CustomWordAllow implements IWordAllow {
    @Override
    public List<String> allow() {
        return Collections.emptyList();
    }
}
