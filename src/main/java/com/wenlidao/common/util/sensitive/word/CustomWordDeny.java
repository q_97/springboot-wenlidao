package com.wenlidao.common.util.sensitive.word;

import com.github.houbb.sensitive.word.api.IWordDeny;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

/**
 * 自定义禁止的词汇
 * @author INYXIN
 * @version 1.0.0
 * 4月 10, 2024   19:13
 */
@Component
public class CustomWordDeny implements IWordDeny {
    @Override
    public List<String> deny() {
        return Collections.emptyList();
    }
}
