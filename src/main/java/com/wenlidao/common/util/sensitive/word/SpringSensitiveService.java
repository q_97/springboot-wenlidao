package com.wenlidao.common.util.sensitive.word;

import com.github.houbb.sensitive.word.bs.SensitiveWordBs;
import com.wenlidao.common.exception.AppException;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SpringSensitiveService {
    @Resource
    public SensitiveWordBs bs;

    /**
     * 检查敏感
     * @param content 内容
     */
    public void checkSensitive(String content) {
        final List<String> all = bs.findAll(content);
        if (all.size() > 0) throw new AppException("检测到内容含有敏感词:" + all);
    }
}
