package com.wenlidao.common.config;

import cn.dev33.satoken.interceptor.SaInterceptor;
import com.wenlidao.common.util.SaTokenUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * SpringMVC 配置
 * @author INYXIN
 * @since 1月 18, 2024   14:20
 */
@Configuration
@Slf4j
public class WebConfig implements WebMvcConfigurer {

    /**
     * 添加拦截器
     * @param registry 注册表
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 注册 Sa-Token 拦截器，打开注解式鉴权功能
        if (!SaTokenUtil.testEnvironment) {//如果是测试环境,则关闭注解鉴权
            registry.addInterceptor(new SaInterceptor()).addPathPatterns("/**");
        }
    }

    /**
     * 跨域资源共享配置
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**").allowedOriginPatterns("*") // 允许的域名，可以使用通配符 * 表示允许所有域名
                .allowedMethods("GET", "POST", "PUT", "DELETE") // 允许的 HTTP 方法
                .exposedHeaders("Authorization").allowCredentials(true) // 是否允许发送 Cookie
                .maxAge(3600); // 预检请求的有效期，单位为秒
    }

}
