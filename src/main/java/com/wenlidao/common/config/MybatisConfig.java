package com.wenlidao.common.config;

import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.annotation.MapperScans;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScans({
        @MapperScan("com.wenlidao.dao")
})
public class MybatisConfig {
}
