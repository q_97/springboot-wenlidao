package com.wenlidao.common.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * 系统常量
 * @author INYXIN
 * @version 1.0.0
 * 3月 28, 2024   15:13
 */
@Configuration
public class SystemConstants {
    /**
     * 服务器 IP
     */
    @Value ("${custom.server.ip}")
    public String SERVER_IP;
    /**
     * 服务器端口
     */
    @Value ("${custom.server.port}")
    public String SERVER_PORT;
    /**
     * 前台 IP
     */
    @Value ("${custom.front.ip}")
    public String FRONT_IP;
    /**
     * 前台端口
     */
    @Value ("${custom.front.port}")
    public String FRONT_PORT;

}
