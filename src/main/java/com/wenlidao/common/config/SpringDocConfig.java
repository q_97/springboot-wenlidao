package com.wenlidao.common.config;

import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.SpecVersion;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.Resource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * <pre>
 * <a href="http://localhost:8080/swagger-ui/index.html">SpringDoc</a>
 * Swagger 和 SpringDoc 注解的对应关系如下：
 *
 * @ Api → @Tag
 * @ ApiIgnore → @Parameter(hidden = true) or @Operation(hidden = true) or @Hidden
 * @ ApiImplicitParam → @Parameter
 * @ ApiImplicitParams → @Parameters
 * @ ApiModel → @Schema
 * @ ApiModelProperty(hidden = true) → @Schema(accessMode = READ_ONLY)
 * @ ApiModelProperty → @Schema
 * @ ApiOperation(value = “foo”, notes = “bar”) → @Operation(summary =
 * “foo”, description = “bar”)
 * @ ApiParam → @Parameter
 * @ ApiResponse(code = 404, msg = “foo”) → @ApiResponse(responseCode
 * = “404”, description = “foo”)
 * ————————————————
 *
 *                             版权声明：本文为博主原创文章，遵循 CC 4.0 BY-SA 版权协议，转载请附上原文出处链接和本声明。
 *
 * 原文链接：https://blog.csdn.net/qq_37284798/article/details/130098876
 * </pre>
 */
@Configuration
public class SpringDocConfig  {
    final String VERSION = "v1.0.0";
    final String TITLE = "接口文档";
    final String DESCRIPTION = "接口文档描述";
    final String AUTHOR = "INYXIN";
    final String EMAIL = "1423716216@qq.com";
    final String USER_HOME = "https://gitee.com/INYXIN";

    @Bean
    public OpenAPI openAPI() {

        return new OpenAPI(SpecVersion.V31)
                //设置 API 文档的基本信息，包括标题、描述、版本等。
                .info(new Info()
                        //文档版本
                        .version(VERSION)
                        //配置文档标题
                        .title(TITLE)
                        //配置文档描述
                        .description(DESCRIPTION)
                        //配置联系人
                        .contact(new Contact()
                                .name(AUTHOR)
                                .email(EMAIL)
                                .url(USER_HOME))
                        .license(new License()
                                .name("许可协议")
                                .url("https://springdoc.org")))
                .externalDocs(new ExternalDocumentation()
                        .description("这是额外的描述")
                );
    }
}
