package com.wenlidao.common.config;

import me.zhyd.oauth.config.AuthConfig;
import me.zhyd.oauth.request.AuthGiteeRequest;
import me.zhyd.oauth.request.AuthGithubRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * OAuth2 提供程序
 * @author INYXIN
 * @version 1.0.0
 * 3月 5, 2024   15:47
 */
@Configuration
public class Oauth2Config {
    @Bean ("gitee")
    public AuthGiteeRequest gitee(@Value ("${custom.oauth2.gitee.client-id}") String clientId,
                                  @Value ("${custom.oauth2.gitee.client-secret}") String clientSecret,
                                  @Value ("${custom.oauth2.gitee.redirect-uri}") String redirectUri) {
        return new AuthGiteeRequest(AuthConfig.builder().clientId(clientId).clientSecret(clientSecret).redirectUri(redirectUri).build());
    }

    @Bean ("github")
    public AuthGithubRequest github(@Value ("${custom.oauth2.github.client-id}") String clientId,
                                    @Value ("${custom.oauth2.github.client-secret}") String clientSecret,
                                    @Value ("${custom.oauth2.github.redirect-uri}") String redirectUri) {
        return new AuthGithubRequest(AuthConfig.builder().clientId(clientId).clientSecret(clientSecret).redirectUri(redirectUri).build());
    }
}
