package com.wenlidao.common.config;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;

/**
 * Spring缓存配置
 * @author INYXIN
 * @version 1.0.0
 * 3月 4, 2024   23:21
 */
@Configuration
@EnableCaching
public class CacheConfig {
    @Bean
    public CacheManager cacheManager(RedisConnectionFactory factory) {
        // 程序关闭,缓存清空
        return new ConcurrentMapCacheManager();
    }
}
