package com.wenlidao.common.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
@Schema (description = "用户注册表单")
public class UserRegisterForm {
    @Schema (description = "用户名")
    @NotBlank(message =  "用户名不能为空")
    private String username;
    @Schema (description = "密码")
    @NotBlank(message =  "密码不能为空")
    private String password;
    @Schema (description = "昵称")
    @NotBlank(message =  "昵称不能为空")
    private String nickname;
    @Schema (description = "验证码")
    @NotBlank(message =  "验证码不能为空")
    private String captcha;
    @Schema (description = "验证码UUID")
    @NotBlank(message =  "验证码UUID不能为空")
    private String uuid;
}
