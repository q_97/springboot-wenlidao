package com.wenlidao.common.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

/**
 * 用户登录表单
 * @author INYXIN
 * @since 1月 30, 2024   20:54
 */
@Data
@Schema (description = "用户名登录表单")
public class UsernameLoginForm {
    @NotBlank(message = "用户名不能为空")
    @Schema (description = "用户名")
    String username;
    @NotBlank(message = "密码不能为空")
    @Schema (description = "登录密码")
    String password;
    @NotBlank(message = "验证码不能为空")
    @Schema (description = "验证码")
    String captcha;
    @NotBlank(message = "验证码的uuid不能为空")
    @Schema (description = "验证码的uuid")
    String uuid;
    @Schema (description = "记住我")
    boolean rememberMe;
}