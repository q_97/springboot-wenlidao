package com.wenlidao.common.dto;

import lombok.Data;

@Data
public class UserFormDTO {
    private Integer pageSize;
    private Integer pageNum;
    private String username;
}
