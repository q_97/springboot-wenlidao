package com.wenlidao.common.dto;

import lombok.Data;

@Data
public class UpdatePasswordDTO {
    private String password;
    private String newPassword;
    private String confirmPassword;
}
