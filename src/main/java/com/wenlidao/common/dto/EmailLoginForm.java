package com.wenlidao.common.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema (description = "邮箱登录表单")
@Data
public class EmailLoginForm {
    @Schema (description = "邮箱")
    private String email;
    @Schema (description = "验证码")
    private String captcha;
    @Schema (description = "记住我")
    boolean rememberMe;
}
