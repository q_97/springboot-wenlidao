package com.wenlidao.common.dto;

import lombok.Data;

@Data
public class CourseTableQueryDTO {
    private String category;
    private String type;
    private String subject;
    private String schoolLeve;
    private String courseName;
    private int pageSize;
    private int pageNum;
}
