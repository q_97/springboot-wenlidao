package com.wenlidao.common.exception;

import cn.dev33.satoken.exception.NotLoginException;
import cn.dev33.satoken.exception.SaTokenException;
import com.wenlidao.common.api.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.io.IOException;

/**
 * Sa-token 令牌异常捕获
 * @author INYXIN
 * @since 1月 30, 2024   02:03
 */
@RestControllerAdvice
@Slf4j
@Order (2)
public class SaTokenExceptionHandler {
    /**
     * <pre>处理程序未登录异常</pre>
     * <a href="https://sa-token.cc/doc.html#/fun/not-login-scene">参考教程</a>
     * @param e NLE系列
     * @return {@link R }
     */
    @ExceptionHandler (NotLoginException.class)
    public R<Void> handlerNotLoginException(NotLoginException e) throws IOException {
        return R.fail().msg(e.getMessage()).put("NOT_LOGIN",true);
    }

    @ExceptionHandler (SaTokenException.class)
    R<Void> SaTokenException(SaTokenException e) {
        log.error("Sa-token : {} , msg= {}", e.getClass(), e.getLocalizedMessage());
        return R.fail().msg(e.getLocalizedMessage());
    }
}
