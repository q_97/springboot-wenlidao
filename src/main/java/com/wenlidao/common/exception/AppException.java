package com.wenlidao.common.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 自定义应用异常
 * @author INYXIN
 * @since 1月 30, 2024   01:37
 */
@EqualsAndHashCode (callSuper = true)
@Data
public class AppException extends RuntimeException {
    /**
     * 自定义应用程序异常
     */
    public AppException() {
        super("操作失败");
    }

    /**
     * 自定义应用程序异常
     * @param message 异常信息
     */
    public AppException(String message) {
        super(message);
    }
}
