package com.wenlidao.common.exception;

import com.wenlidao.common.api.R;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.sql.SQLIntegrityConstraintViolationException;

/**
 * 捕获mybatis中产生的异常
 * @author INYXIN
 * @version 1.0.0
 * 4月 6, 2024   21:11
 */
@RestControllerAdvice
@Order (2)
public class MyBatisExceptionHandler {
    @ExceptionHandler (SQLIntegrityConstraintViolationException.class)
    public R<Void> SQLIntegrityConstraintViolationException(SQLIntegrityConstraintViolationException e) {
        e.printStackTrace();//打印日志,方便查找问题
        return R.fail().msg("存在外键关联，数据约束异常,请检查数据的正确性");
    }
}
