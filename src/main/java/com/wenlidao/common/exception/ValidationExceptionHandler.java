package com.wenlidao.common.exception;


import com.wenlidao.common.api.R;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.core.annotation.Order;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.HandlerMethodValidationException;

import java.util.List;
import java.util.Set;

/**
 * 参数验证异常全局处理
 * @author 就眠仪式
 */
@Order (1)
@RestControllerAdvice
public class ValidationExceptionHandler {

    /**
     * 处 理 form data 方 式 调 用 接 口 校 验 失 败 抛 出 的 异 常
     */
    @ExceptionHandler (BindException.class)
    public R<Void> bindExceptionHandler(BindException e) {
        List<FieldError> fieldErrors = e.getBindingResult().getFieldErrors();
        List<String> collect = fieldErrors.stream().map(DefaultMessageSourceResolvable::getDefaultMessage).toList();
        return R.error().msg(collect.toString());
    }

    /**
     * 处 理 json 请 求 体 调 用 接 口 校 验 失 败 抛 出 的 异 常
     */
    @ExceptionHandler (MethodArgumentNotValidException.class)
    public R<Void> methodArgumentNotValidExceptionHandler(MethodArgumentNotValidException e) {
        List<FieldError> fieldErrors = e.getBindingResult().getFieldErrors();
        List<String> collect = fieldErrors.stream().map(DefaultMessageSourceResolvable::getDefaultMessage).toList();
        return R.error().msg(collect.toString());
    }

    /**
     * 处 理 单 个 参 数 校 验 失 败 抛 出 的 异 常
     */
    @ExceptionHandler (ConstraintViolationException.class)
    public R<Void> constraintViolationExceptionHandler(ConstraintViolationException e) {
        Set<ConstraintViolation<?>> constraintViolations = e.getConstraintViolations();
        List<String> collect = constraintViolations.stream().map(ConstraintViolation::getMessage).toList();
        return R.error().msg(collect.toString());
    }


    @ExceptionHandler (HttpMessageNotReadableException.class)
    public R<Void> HttpMessageNotReadableException(HttpMessageNotReadableException e) {
        return R.error().msg("参数解析失败, 请检查参数类型. ").put("details", e.getLocalizedMessage());
    }

    @ExceptionHandler (HandlerMethodValidationException.class)
    public R<Void> HandlerMethodValidationException(HandlerMethodValidationException e) {
        return R.error().msg(e.getMessage());
    }
}
