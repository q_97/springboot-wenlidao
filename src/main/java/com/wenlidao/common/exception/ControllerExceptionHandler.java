package com.wenlidao.common.exception;


import com.wenlidao.common.api.R;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.connector.ClientAbortException;
import org.springframework.core.io.UrlResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.resource.NoResourceFoundException;

import java.io.IOException;
import java.util.Arrays;


/**
 * 控制器全局异常捕获
 * @author INYXIN
 * @since 1月 30, 2024   01:45
 */
@RestControllerAdvice
@Slf4j
public class ControllerExceptionHandler {
    /**
     * 不 支 持 的 请 求 类 型
     */
    @ExceptionHandler ( {HttpRequestMethodNotSupportedException.class})
    public R<Void> handleException(HttpRequestMethodNotSupportedException e) {
        log.error(e.getMessage(), e);
        return R.error().msg("不支持的请求类型" + e.getMethod());
    }

    @ExceptionHandler (HttpMediaTypeException.class)
    public R<Void> HttpMediaTypeException(HttpMediaTypeException e) {
        log.error("不支持当前媒体类型");
        return R.error().msg("不支持当前媒体类型" + Arrays.toString(e.getDetailMessageArguments()));
    }

    //异常处理器要从上往下排序 上面的处理器有限处理
    @ExceptionHandler (NoResourceFoundException.class)
    public R<Void> NoResourceFoundException(NoResourceFoundException e, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        log.error("资源未找到 404  [{}]", request.getRequestURI());
        return R.error().msg(e.getLocalizedMessage());
    }

    @ExceptionHandler (ServletRequestBindingException.class)
    public R<Void> ServletRequestBindingException(ServletRequestBindingException e) {
        log.error("参数绑定异常 {}", e.getMessage());
        return R.error().msg("参数绑定异常"+e.getLocalizedMessage());
    }

    @ExceptionHandler (NullPointerException.class)
    public R<Void> NullPointerException(NullPointerException e) {
        log.error("空指针异常 :{} " + e.getMessage());
        e.printStackTrace();
        return R.error().msg(e.getLocalizedMessage());
    }

    @ExceptionHandler(ClientAbortException.class)
    public ResponseEntity<UrlResource> ClientAbortException(ClientAbortException e, HttpServletResponse response){
        log.error("请求被中断，可能是客户端主动断开连接");
        return ResponseEntity.notFound().build();
    }

    /**
     * 自定义应用程序异常,所有业务中抛出的AppException异常都会被这个处理器处理
     * @param e e
     * @return {@link R }<{@link Void }>
     */
    @ExceptionHandler (AppException.class)
    public R<Void> AppException(AppException e) {
        log.error("业务异常: {}",  e.getMessage());
        return R.fail().msg(e.getLocalizedMessage());
    }

    @ExceptionHandler (Exception.class)
    public R<Void> handleException(Exception e) throws Exception {
        e.printStackTrace();
        log.error("未知的错误 exception: {} {}", e.getClass(), e.getMessage());
        System.err.println("请补充异常处理");
        return R.error().msg(e.getLocalizedMessage());
    }

}
