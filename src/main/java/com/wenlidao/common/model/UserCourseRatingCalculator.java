package com.wenlidao.common.model;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wenlidao.dao.CourseActionDao;
import com.wenlidao.entity.model.UserActivity;
import com.wenlidao.service.course.action.ActionService;
import com.wenlidao.service.course.action.IssueService;
import com.wenlidao.service.course.action.ReplyService;
import com.wenlidao.service.course.action.SelectedCourseService;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;

/**
 * 功能：通过用户行为权重比计算用户对当前课程评分
 * 作者：jfcjjcddmd
 * 日期：2024/4/26 10:45
 */
@Slf4j
public class UserCourseRatingCalculator {

    public double calculateRating(UserActivity userActivity,Long maxViewCount,Long maxIssueCount,Long maxReplyCount,Long maxSelectCount) {
        double ANSWER_WEIGHT = 0.25;
        double QUESTION_WEIGHT = 0.15;
        double SELECT_WEIGHT = 0.45;
        double VIEW_WEIGHT = 0.15;
        int MAX_VIEW_COUNT = Math.toIntExact(maxViewCount);
        int MAX_SELECTED_COURSES = Math.toIntExact(maxSelectCount);
        int MAX_QUESTIONS_ASKED = Math.toIntExact(maxIssueCount);
        int MAX_ANSWERS_PROVIDED = Math.toIntExact(maxReplyCount);


        int viewCount = userActivity.getViewCount();
        int selectedCourses = userActivity.getSelectedCourses();
        int questionsAsked = userActivity.getQuestionsAsked();
        int answersProvided = userActivity.getAnswersProvided();

        double normalizedViewScore = normalize(viewCount, MAX_VIEW_COUNT);
        double normalizedSelectScore = normalize(selectedCourses, MAX_SELECTED_COURSES);
        double normalizedQuestionScore = normalize(questionsAsked, MAX_QUESTIONS_ASKED);
        double normalizedAnswerScore = normalize(answersProvided, MAX_ANSWERS_PROVIDED);


        double totalScore =
                VIEW_WEIGHT * normalizedViewScore +
                        SELECT_WEIGHT * normalizedSelectScore +
                        QUESTION_WEIGHT * normalizedQuestionScore +
                        ANSWER_WEIGHT * normalizedAnswerScore;

        // 映射到1-5分的评分范围

        return Math.min(5.0, Math.max(1.0, totalScore * 5));
    }

    private double normalize(int score, int maxScore) {
        return (double) score / maxScore;
    }


}
