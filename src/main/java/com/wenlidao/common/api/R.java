package com.wenlidao.common.api;

import com.alibaba.fastjson2.JSONObject;
import com.github.pagehelper.PageInfo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.List;


/*
  通用响应结果
    @author INYXIN
 * @since 1月 16, 2024   15:40
 */


@Slf4j
@Setter
@Accessors (chain = true,fluent = true)
@Schema (description = "通用响应结果")
public class R<T> {
    @Schema (description = "响应状态码")
    public int code;
    @Schema (description = "响应数据")
    public T data;
    @Schema (description = "响应消息")
    public String msg;
    @Schema (description = "响应时间")
    public Long time;
    @Schema (description = "扩展数据")
    public HashMap<String, Object> ext = new HashMap<>();

    public R<T> put(String key, Object value) {
        this.ext.put(key, value);
        return this;
    }

    private R(int code, T data, String msg) {
        this.code = code;
        this.data = data;
        this.msg = msg;
        this.time(System.currentTimeMillis());
    }

    public static <T> R<T> success() {
        return new R<>(0, null, "OK");
    }

    public static <T> R<T> success(T data) {
        return new R<>(0, data, "OK");
    }

    public static <T> R<List<T>> success(List<T> data, PageInfo<T> pageInfo) {
        return new R<>(0, data, "OK")
                .put("size", pageInfo.getPageSize())
                .put("page", pageInfo.getPageNum())
                .put("total", pageInfo.getTotal());
    }

    public static R<Void> fail() {
        return new R<>(1, null, "响应失败!");
    }

    public static R<Void> error() {
        return new R<>(2, null, "系统异常!");
    }

    public String toJson() {
        return JSONObject.toJSONString(this);
    }

}