import axios from "axios";
import {Message} from "element-ui";
import store, {getInfo} from "@/store";
import {useRouter} from "vue-router/composables";

const TOKEN_HEADER = 'Authorization';
const SUCCESS_CODE = 0;
const FAIL_CODE = 1;
const ERROR_CODE = 2

function onErrorMsg(code, msg) {
    if (code === FAIL_CODE) {
        Message({message: msg || 'Warn', type: 'warning'})
    } else if (code === ERROR_CODE) {
        Message({message: msg || 'Error', type: 'error'})
    }
}

export const baseURL = 'http://localhost:8080';

// 创建axios实例
let request = axios.create({
    baseURL, withCredentials: false, // 跨域请求时是否需要访问凭证
    timeout: 60 * 1000 * 3, // 请求超时时间
});

//请求拦截器
request.interceptors.request.use(config => {
    //todo 发送请求前请携带token
    //例如   config.headers.setAuthorization('Bearer ' + localStorage.getItem('token'))
    store.token.getToken()
    config.headers.setAuthorization('Bearer ' + getInfo().token?.tokenValue)
    return config;
})
//响应拦截器
request.interceptors.response.use(response => {
        let {code, data, msg, ext} = response.data;
        if (code !== SUCCESS_CODE)
            onErrorMsg(code, msg)
        if (ext.NOT_LOGIN === true) {
            localStorage.removeItem("info")
            location.href = '/'
        }
        return {code, data, msg, ext};
    }, error => {
        onErrorMsg(ERROR_CODE, error.message)
    }
)
export default request