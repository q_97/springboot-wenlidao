import {reactive} from "vue";

const token = {
    key: "wenlidao.token",
    getToken() {
        return localStorage.getItem(this.key)
    },
    setToken(token) {
        localStorage.setItem(this.key, token)
    }
}

const user = {
    "id": 1,
    "username": "b96c08d8-9201-4821-a796-25e0ec8a0984",
    "nickname": "INYXIN",
    "password": "$2a$10$kGYB9AjieXdKxYaTB0ay/O6XF/8t5PfGGDpiP5aAoCD7ap0rNdlDK",
    "gender": null,
    "role": {
        "label": "管理员", "value": "Admin"
    },
    "location": null,
    "remark": "空空如也,来说点什么吧~",
    "avatar": "https://foruda.gitee.com/avatar/1701619178492802862/12060036_inyxin_1701619178.png",
    "birthday": null,
    "disabled": false,
    "accounts": {
        "uu": "1234", "uu2": "1234123", "GITEE": "12060036"
    },
    setUser(user) {
        localStorage.setItem("wenlidao.user", JSON.stringify(user))
    }
}
/**
 * 简易的全局状态管理器
 * */
export default reactive({user, token})

export function getInfo() {
    let item = localStorage.getItem("info");
    if (!item) {
        item = JSON.stringify({user: {}, token: {isLogin: false}})
    }
    let {user, token} = JSON.parse(item);
    return {user, token}
}

export function setInfo({user, token}) {
    localStorage.setItem("info", JSON.stringify({user, token}))
}

export function getActive() {
    return localStorage.getItem('getActive')
}

export function setActive(active) {
    localStorage.setItem('getActive',active)
}
