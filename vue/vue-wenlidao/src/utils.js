import {MessageBox} from 'element-ui';

const utils = {
    /**
     * 封装了element的$confirm方法
     * @param title
     * @returns {Promise<unknown>}
     */
    async confirm(title) {
        return await new Promise((resolve, reject) => {
            MessageBox.confirm(title, '提示', {
                confirmButtonText: '确定', cancelButtonText: '取消', type: 'warning'
            }).then(() => {
                resolve(true)
            }).catch(() => {
                resolve(false)
            });
        })
    }
}

export default utils;






