import Vue from 'vue';
import ElementUI from 'element-ui';

import 'element-ui/lib/theme-chalk/index.css';
import './assets/css/login.css';
import './style/base.css'
import '@/assets/css/custom.css' /*css 和静态资源文件尽量放入 @/assets 目录下*/
import App from './App.vue';
import router from './router';
import './assets/iconfont/iconfont.js'
import './westeros.json'
import axios from 'axios';
import * as echarts from 'echarts'
import  westeros from './westeros.json'
echarts.registerTheme('westeros',westeros);

// 进行全局配置
axios.defaults.baseURL = 'http://localhost:8080/'; // 你的API基础URL
//设置请求超时时间
axios.defaults.timeout = 5000;

// 配置Axios并将其添加到Vue.prototype上，这样可以在组件内通过this.axios访问
Vue.prototype.$axios = axios;

Vue.use(ElementUI);
Vue.config.productionTip = false

new Vue({
    el: '#app',
    router,
    render: h => h(App)
})
