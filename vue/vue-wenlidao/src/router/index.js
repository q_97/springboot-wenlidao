import Vue from 'vue'
import VueRouter from 'vue-router'
import {getInfo} from "@/store";

Vue.use(VueRouter)

const routes = [
    /*---------------------------------------INYXIN--------------------------------------*/
    {name: "messagePage", path: '/message', component: () => import('@/views/message/MessagePage')},
    {
        name: "courseHome",
        path: "/course/h/:courseId",
        component: () => import("@/views/courseDescPage/CourseDescPage.vue")
    },
    {
        name: "courseItem",
        path: "/course/v/:courseId",
        component: () => import("@/views/coursePlayPage/CoursePlayPage.vue"),
    },
    /*---------------------------------------INYXIN--------------------------------------*/
    /*---------------------------------------HJY-----------------------------------------*/
    {
        name: "home", path: "/",
        component: () => import("@/views/FrontView.vue")
    },
    {
        name: "front", path: "/front",
        component: () => import("@/views/FrontView.vue")
    },
    {
        name: "loginTop", path: "/loginTop",
        component: () => import("@/components/personTop.vue")
    },
    {
        name: "person", path: "/person",
        component: () => import("@/views/PersonView.vue")
    },
    {
        name: "count", path: "/count",
        component: () => import("@/views/CountSetting.vue"),
        meta: {isAuth: true},
        beforeEnter: (to, from, next) => {
            if (to.meta.isAuth) { //判断是否需要授权
                if (getInfo().token['isLogin'] === true) {
                    next()  //放行
                } else {
                    location.href = '/'
                }
            } else {
                next()  //放行
            }
        }
    },
    {
        name: "updatePassword", path: "/updatePassword",
        component: () => import("@/views/UpdatePassword.vue"),
        meta: {isAuth: true},
        beforeEnter: (to, from, next) => {
            if (to.meta.isAuth) { //判断是否需要授权
                if (getInfo().token['isLogin'] === true) {
                    next()  //放行
                } else {
                    location.href = '/'
                }
            } else {
                next()  //放行
            }
        }
    },
    /*---------------------------------------HJY-----------------------------------------*/
    {
        name: "ClassifyPage",
        path: "/course/classify",
        component: () => import("@/views/ClassifyPage.vue")
    },
    {
        name: "SearchPage",
        path: "/course/search",
        component: () => import("@/views/SearchPage.vue")
    },
    {
        name: "login",
        path: "/login",
        component: () => import("@/views/login/LoginView.vue")
    },
    {
        name: "register",
        path: "/register",
        component: () => import("@/views/login/RegisterView.vue")
    },
    {
        path: '/manager',
        name: 'Manager',
        component: () => import('@/views/background/Manager.vue'),
        redirect: '/manager/home',  // 重定向到主页
        children: [
            {path: '403', name: 'NoAuth', meta: {name: '无权限'}, component: () => import('@/views/background/403')},
            {
                path: 'home',
                name: 'Home',
                meta: {name: '系统首页'},
                component: () => import('@/views/background/Home'),
                beforeEnter: (to, from, next) => {
                    if (to.meta.name === '系统首页') { //判断是否需要授权
                        if (getInfo().token['isLogin'] === true && getInfo().user.role.value === 'Admin') {
                            next()  //放行
                        } else {
                            location.href = '/'
                        }
                    } else {
                        next()  //放行
                    }
                },
            },
            {
                path: 'admin',
                name: 'Admin',
                meta: {name: '管理员信息'},
                component: () => import('@/views/background/Admin')
            },
            {
                path: 'adminPerson',
                name: 'AdminPerson',
                meta: {name: '个人信息'},
                component: () => import('@/views/background/AdminPerson')
            },
            {
                path: 'password',
                name: 'Password',
                meta: {name: '修改密码'},
                component: () => import('@/views/background/Password')
            },
            {
                path: 'notice',
                name: 'Notice',
                meta: {name: '公告信息'},
                component: () => import('@/views/background/Notice')
            },
            {path: 'user', name: 'User', meta: {name: '用户管理'}, component: () => import('@/views/background/User')},
            {
                path: 'course',
                name: 'Course',
                meta: {name: '课程信息'},
                component: () => import('@/views/background/Course')
            },
        ]
    }


]

const router = new VueRouter({
    mode: 'history',//默认是hash,去除#号
    routes
})

export default router
